﻿CREATE PROC uspInsertInto_tbCustomers
AS
INSERT INTO tbCustomers (CustomerID, FirstName, LastName, HomeAddress, PhoneNumber, EmailAddress, CouponCode)
VALUES 
(1,'Jane','Fonda','54 jksja Street, Dublin 5','085-2548-254', 'jane@gmail.com','ABC123'), 
(2,'Harold','Hedge','54 Blue Street, Dublin 4','089-2542-358', 'harold@gmail.com', 'ABC123'),
(3,'Jeniffer','Aniston','67 Ashton Street, Dublin 12','085-2254-254', 'jeniffer@gmail.com','ABC123' ),
(4,'Leopold','Perk','89 Arrow Street, Dublin 4','085-2548-457', 'leopold@gmail.com','BBC123' ),
(5,'Jeremy','Irons','132 Hedge Street, Dublin 4','085-1478-254', 'jeremy@gmail.com','BBC123' )