﻿CREATE PROC Create_tbCustomers
AS
CREATE TABLE tbCustomers(
CustomerID int not null PRIMARY KEY,
FirstName varchar (100),
LastName varchar(50),
HomeAddress varchar(200),
PhoneNumber varchar (20),
EmailAddress varchar(50),
CouponCode varchar(10)
)