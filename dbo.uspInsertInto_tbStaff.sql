﻿
CREATE PROC uspInsertInto_tbStaff
AS
INSERT INTO tbStaff (StaffID, StaffFirstName, StaffLastName, StaffAddress, StaffPhone, StaffEmail, StaffPassword, AccessLevel)
VALUES
(1,'Jack','Nicholson','54 Pottery Road, Dublin 5','085-2548-254', 'jacknicholson@gmail.com','000000', 'staff' ),
(2,'Joe','Doe','54 Pearse Street, Dublin 4','089-2542-358', 'joedoe@gmail.com', '000000', 'staff'), 
(3,'Tom','Perk','67 Patrick Street, Dublin 12','085-2254-254', 'tomperk@gmail.com', '000000', 'staff' ),
(4,'Peter','Parker','89 East Courtyard, Dublin 4','085-2548-457', 'peterparker@gmail.com', '000000', 'staff' ),
(10000,'James','Potter','132 Westland Row, Dublin 4','085-1478-254', 'jamespotter@gmail.com', '000000','admin')