﻿CREATE PROC uspInsertInto_tbJobs
AS
INSERT INTO tbJobs (JobID, JobDescription)
VALUES (1,'Full Service'),
 (2,'Minor Service'),
 (3,'Pre-NCT Inspection'),
 (4,'Replace tyres'),
 (5,'Vehicle Recovery'),
 (6,'Windscreen Replacement'),
 (7,'Timing Belt Replacement'),
 (8,'CV-Boot Replacement'),
 (9,'Axle Realignment'),
 (10,'Body-shop Repair'),
 (11,'Engine Repair'),
 (12,'General Repair')