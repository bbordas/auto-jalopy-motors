﻿CREATE PROC Create_tbJobCards
AS
CREATE TABLE tbJobCards(
CardID int not null PRIMARY KEY,
StartDate date,
EndDate date,
CarID int,
JobID int,
StaffID int

FOREIGN KEY (CarID) REFERENCES tbCars(CarID),
FOREIGN KEY (JobID) REFERENCES tbJobs(JobID),
FOREIGN KEY (StaffID) REFERENCES tbStaff(StaffID)
)