﻿CREATE PROC Create_tbCars
AS
CREATE TABLE tbCars(
CarID int  not null PRIMARY KEY,
RegNum varchar (10),
Make varchar (100),
Model varchar(50),
Color varchar(200),
NCT_Due date,
CustomerID int

FOREIGN KEY (CustomerID) REFERENCES tbCustomers(CustomerID)
)