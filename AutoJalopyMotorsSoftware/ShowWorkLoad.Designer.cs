﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmShowWorkLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvWorkLoad = new System.Windows.Forms.DataGridView();
            this.btnWorkLoadStaff = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.dtpEndWL = new System.Windows.Forms.DateTimePicker();
            this.dtpStartWL = new System.Windows.Forms.DateTimePicker();
            this.cmbStaffI = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkLoad)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvWorkLoad
            // 
            this.dgvWorkLoad.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvWorkLoad.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkLoad.Location = new System.Drawing.Point(12, 12);
            this.dgvWorkLoad.Name = "dgvWorkLoad";
            this.dgvWorkLoad.Size = new System.Drawing.Size(360, 239);
            this.dgvWorkLoad.TabIndex = 0;
            // 
            // btnWorkLoadStaff
            // 
            this.btnWorkLoadStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWorkLoadStaff.Location = new System.Drawing.Point(242, 320);
            this.btnWorkLoadStaff.Name = "btnWorkLoadStaff";
            this.btnWorkLoadStaff.Size = new System.Drawing.Size(130, 29);
            this.btnWorkLoadStaff.TabIndex = 10;
            this.btnWorkLoadStaff.Text = "WorkLoad";
            this.btnWorkLoadStaff.UseVisualStyleBackColor = true;
            this.btnWorkLoadStaff.Click += new System.EventHandler(this.btnWorkLoadStaff_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(240, 272);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 20);
            this.label20.TabIndex = 60;
            this.label20.Text = "-";
            // 
            // dtpEndWL
            // 
            this.dtpEndWL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEndWL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndWL.Location = new System.Drawing.Point(261, 269);
            this.dtpEndWL.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpEndWL.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpEndWL.Name = "dtpEndWL";
            this.dtpEndWL.Size = new System.Drawing.Size(111, 26);
            this.dtpEndWL.TabIndex = 59;
            // 
            // dtpStartWL
            // 
            this.dtpStartWL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStartWL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartWL.Location = new System.Drawing.Point(126, 269);
            this.dtpStartWL.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpStartWL.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpStartWL.Name = "dtpStartWL";
            this.dtpStartWL.Size = new System.Drawing.Size(106, 26);
            this.dtpStartWL.TabIndex = 58;
            // 
            // cmbStaffI
            // 
            this.cmbStaffI.FormattingEnabled = true;
            this.cmbStaffI.Location = new System.Drawing.Point(44, 271);
            this.cmbStaffI.Name = "cmbStaffI";
            this.cmbStaffI.Size = new System.Drawing.Size(46, 21);
            this.cmbStaffI.TabIndex = 61;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 18);
            this.label4.TabIndex = 62;
            this.label4.Text = "ID: ";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(12, 320);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 29);
            this.btnCancel.TabIndex = 63;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmShowWorkLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbStaffI);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.dtpEndWL);
            this.Controls.Add(this.dtpStartWL);
            this.Controls.Add(this.btnWorkLoadStaff);
            this.Controls.Add(this.dgvWorkLoad);
            this.Name = "frmShowWorkLoad";
            this.Text = "ShowWorkLoad";
            this.Load += new System.EventHandler(this.frmShowWorkLoad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkLoad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvWorkLoad;
        private System.Windows.Forms.Button btnWorkLoadStaff;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker dtpEndWL;
        private System.Windows.Forms.DateTimePicker dtpStartWL;
        private System.Windows.Forms.ComboBox cmbStaffI;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCancel;
    }
}