﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {

            string userName = txtID.Text;
            string password = txtPassword.Text;

            if (IsValidLogin(userName, password))
            {
                using (AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext())
                {
                    txtID.Clear();
                    txtPassword.Clear();

                    try
                    {
                        var accesslevel = (from s in AutoJDC.tbStaffs
                                           where s.StaffEmail.Equals(userName) &&
                                           s.StaffPassword.Equals(password)
                                           select new { s.AccessLevel }).Single();

                        if (accesslevel.AccessLevel == "admin")
                        {
                            using (Admin adminForm = new Admin())
                            {
                                adminForm.ShowDialog();

                            }
                        }
                        if (accesslevel.AccessLevel == "staff")
                        {
                            using (frmStaff staffForm = new frmStaff())
                            {
                                staffForm.ShowDialog();

                            }

                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Invalid Login");
                    }
                }
            }
            else
                MessageBox.Show("Invalid Login");
        }

        public static bool IsValidLogin(string userName, string password)
        {
            using (AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext())
            {
                var staff = (from s in AutoJDC.tbStaffs
                             where s.StaffEmail.Equals(userName) &&
                             s.StaffPassword.Equals(password)
                             select s);
                if (staff != null)
                {
                    return true;
                }
                else return false;
            }
        }
    }
}

