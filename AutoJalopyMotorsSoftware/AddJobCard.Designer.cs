﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmAddCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbRegNum = new System.Windows.Forms.ComboBox();
            this.dtpExpectedDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.cmbMechanic = new System.Windows.Forms.ComboBox();
            this.cmbJob = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnAddJobCard = new System.Windows.Forms.Button();
            this.txtJObCardID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtJObCardID);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.btnCancel);
            this.groupBox4.Controls.Add(this.cmbRegNum);
            this.groupBox4.Controls.Add(this.dtpExpectedDate);
            this.groupBox4.Controls.Add(this.dtpStartDate);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.cmbMechanic);
            this.groupBox4.Controls.Add(this.cmbJob);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.btnAddJobCard);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(26, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(332, 337);
            this.groupBox4.TabIndex = 31;
            this.groupBox4.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(23, 287);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 29);
            this.btnCancel.TabIndex = 51;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
            // 
            // cmbRegNum
            // 
            this.cmbRegNum.FormattingEnabled = true;
            this.cmbRegNum.Items.AddRange(new object[] {
            "Full Service",
            "Minor Service",
            "Pre-NCT Inspection",
            "Replace tyres",
            "Vehicle Recovery",
            "Windscreen Replacement",
            "Timing Belt Replacement",
            "CV-Boot Replacement",
            "Axle Realignment",
            "Body-shop Repair",
            "Engine Repair",
            "General Repair"});
            this.cmbRegNum.Location = new System.Drawing.Point(130, 104);
            this.cmbRegNum.Name = "cmbRegNum";
            this.cmbRegNum.Size = new System.Drawing.Size(171, 26);
            this.cmbRegNum.TabIndex = 48;
            // 
            // dtpExpectedDate
            // 
            this.dtpExpectedDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpExpectedDate.Location = new System.Drawing.Point(130, 228);
            this.dtpExpectedDate.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpExpectedDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpExpectedDate.Name = "dtpExpectedDate";
            this.dtpExpectedDate.Size = new System.Drawing.Size(171, 26);
            this.dtpExpectedDate.TabIndex = 47;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartDate.Location = new System.Drawing.Point(130, 188);
            this.dtpStartDate.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpStartDate.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(171, 26);
            this.dtpStartDate.TabIndex = 46;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 236);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(79, 18);
            this.label27.TabIndex = 45;
            this.label27.Text = "End-Date:";
            // 
            // cmbMechanic
            // 
            this.cmbMechanic.FormattingEnabled = true;
            this.cmbMechanic.Location = new System.Drawing.Point(130, 146);
            this.cmbMechanic.Name = "cmbMechanic";
            this.cmbMechanic.Size = new System.Drawing.Size(171, 26);
            this.cmbMechanic.TabIndex = 43;
            // 
            // cmbJob
            // 
            this.cmbJob.FormattingEnabled = true;
            this.cmbJob.Items.AddRange(new object[] {
            "Full Service",
            "Minor Service",
            "Pre-NCT Inspection",
            "Replace tyres",
            "Vehicle Recovery",
            "Windscreen Replacement",
            "Timing Belt Replacement",
            "CV-Boot Replacement",
            "Axle Realignment",
            "Body-shop Repair",
            "Engine Repair",
            "General Repair"});
            this.cmbJob.Location = new System.Drawing.Point(130, 62);
            this.cmbJob.Name = "cmbJob";
            this.cmbJob.Size = new System.Drawing.Size(171, 26);
            this.cmbJob.TabIndex = 41;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(86, 18);
            this.label19.TabIndex = 29;
            this.label19.Text = "Select Job:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(20, 191);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 18);
            this.label23.TabIndex = 17;
            this.label23.Text = "Start Date:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(20, 149);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 18);
            this.label24.TabIndex = 12;
            this.label24.Text = "Mechanic:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(20, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 18);
            this.label25.TabIndex = 11;
            this.label25.Text = "Reg Number:";
            // 
            // btnAddJobCard
            // 
            this.btnAddJobCard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddJobCard.Location = new System.Drawing.Point(171, 287);
            this.btnAddJobCard.Name = "btnAddJobCard";
            this.btnAddJobCard.Size = new System.Drawing.Size(130, 29);
            this.btnAddJobCard.TabIndex = 9;
            this.btnAddJobCard.Text = "Ok";
            this.btnAddJobCard.UseVisualStyleBackColor = true;
            this.btnAddJobCard.Click += new System.EventHandler(this.btnAddJobCard_Click);
            // 
            // txtJObCardID
            // 
            this.txtJObCardID.Location = new System.Drawing.Point(130, 25);
            this.txtJObCardID.Name = "txtJObCardID";
            this.txtJObCardID.Size = new System.Drawing.Size(171, 26);
            this.txtJObCardID.TabIndex = 53;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(20, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 52;
            this.label14.Text = "ID:";
            // 
            // frmAddCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.groupBox4);
            this.Name = "frmAddCard";
            this.Text = "AddJobCard";
            this.Load += new System.EventHandler(this.frmAddCard_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cmbRegNum;
        private System.Windows.Forms.DateTimePicker dtpExpectedDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmbMechanic;
        private System.Windows.Forms.ComboBox cmbJob;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnAddJobCard;
        private System.Windows.Forms.TextBox txtJObCardID;
        private System.Windows.Forms.Label label14;
    }
}