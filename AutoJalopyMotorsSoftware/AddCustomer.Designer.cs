﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmAddCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbNewCustomer = new System.Windows.Forms.GroupBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCustomerCoupon = new System.Windows.Forms.TextBox();
            this.TxtCustomerEmail = new System.Windows.Forms.TextBox();
            this.txtCustomerPhone = new System.Windows.Forms.TextBox();
            this.txtCustomerAddress = new System.Windows.Forms.TextBox();
            this.txtCustomerLastName = new System.Windows.Forms.TextBox();
            this.txtCustomerFirstName = new System.Windows.Forms.TextBox();
            this.lblCoupon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerLN = new System.Windows.Forms.Label();
            this.lblCustomerFN = new System.Windows.Forms.Label();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnCustCancel = new System.Windows.Forms.Button();
            this.grbNewCustomer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbNewCustomer
            // 
            this.grbNewCustomer.Controls.Add(this.btnCustCancel);
            this.grbNewCustomer.Controls.Add(this.txtCustomerID);
            this.grbNewCustomer.Controls.Add(this.label14);
            this.grbNewCustomer.Controls.Add(this.txtCustomerCoupon);
            this.grbNewCustomer.Controls.Add(this.TxtCustomerEmail);
            this.grbNewCustomer.Controls.Add(this.txtCustomerPhone);
            this.grbNewCustomer.Controls.Add(this.txtCustomerAddress);
            this.grbNewCustomer.Controls.Add(this.txtCustomerLastName);
            this.grbNewCustomer.Controls.Add(this.txtCustomerFirstName);
            this.grbNewCustomer.Controls.Add(this.lblCoupon);
            this.grbNewCustomer.Controls.Add(this.lblEmail);
            this.grbNewCustomer.Controls.Add(this.lblPhone);
            this.grbNewCustomer.Controls.Add(this.lblAddress);
            this.grbNewCustomer.Controls.Add(this.lblCustomerLN);
            this.grbNewCustomer.Controls.Add(this.lblCustomerFN);
            this.grbNewCustomer.Controls.Add(this.btnAddCustomer);
            this.grbNewCustomer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNewCustomer.Location = new System.Drawing.Point(20, 3);
            this.grbNewCustomer.Name = "grbNewCustomer";
            this.grbNewCustomer.Size = new System.Drawing.Size(340, 351);
            this.grbNewCustomer.TabIndex = 17;
            this.grbNewCustomer.TabStop = false;
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Location = new System.Drawing.Point(132, 9);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerID.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "ID:";
            // 
            // txtCustomerCoupon
            // 
            this.txtCustomerCoupon.Location = new System.Drawing.Point(132, 260);
            this.txtCustomerCoupon.Name = "txtCustomerCoupon";
            this.txtCustomerCoupon.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerCoupon.TabIndex = 25;
            // 
            // TxtCustomerEmail
            // 
            this.TxtCustomerEmail.Location = new System.Drawing.Point(132, 218);
            this.TxtCustomerEmail.Name = "TxtCustomerEmail";
            this.TxtCustomerEmail.Size = new System.Drawing.Size(171, 26);
            this.TxtCustomerEmail.TabIndex = 24;
            // 
            // txtCustomerPhone
            // 
            this.txtCustomerPhone.Location = new System.Drawing.Point(132, 177);
            this.txtCustomerPhone.Name = "txtCustomerPhone";
            this.txtCustomerPhone.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerPhone.TabIndex = 23;
            // 
            // txtCustomerAddress
            // 
            this.txtCustomerAddress.Location = new System.Drawing.Point(132, 135);
            this.txtCustomerAddress.Name = "txtCustomerAddress";
            this.txtCustomerAddress.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerAddress.TabIndex = 22;
            // 
            // txtCustomerLastName
            // 
            this.txtCustomerLastName.Location = new System.Drawing.Point(132, 93);
            this.txtCustomerLastName.Name = "txtCustomerLastName";
            this.txtCustomerLastName.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerLastName.TabIndex = 21;
            // 
            // txtCustomerFirstName
            // 
            this.txtCustomerFirstName.Location = new System.Drawing.Point(132, 51);
            this.txtCustomerFirstName.Name = "txtCustomerFirstName";
            this.txtCustomerFirstName.Size = new System.Drawing.Size(171, 26);
            this.txtCustomerFirstName.TabIndex = 20;
            // 
            // lblCoupon
            // 
            this.lblCoupon.AutoSize = true;
            this.lblCoupon.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoupon.Location = new System.Drawing.Point(22, 263);
            this.lblCoupon.Name = "lblCoupon";
            this.lblCoupon.Size = new System.Drawing.Size(67, 18);
            this.lblCoupon.TabIndex = 19;
            this.lblCoupon.Text = "Coupon:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(22, 221);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(52, 18);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(22, 180);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(57, 18);
            this.lblPhone.TabIndex = 17;
            this.lblPhone.Text = "Phone:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(22, 138);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(71, 18);
            this.lblAddress.TabIndex = 12;
            this.lblAddress.Text = "Address:";
            // 
            // lblCustomerLN
            // 
            this.lblCustomerLN.AutoSize = true;
            this.lblCustomerLN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerLN.Location = new System.Drawing.Point(22, 96);
            this.lblCustomerLN.Name = "lblCustomerLN";
            this.lblCustomerLN.Size = new System.Drawing.Size(81, 18);
            this.lblCustomerLN.TabIndex = 11;
            this.lblCustomerLN.Text = "Lastname:";
            // 
            // lblCustomerFN
            // 
            this.lblCustomerFN.AutoSize = true;
            this.lblCustomerFN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFN.Location = new System.Drawing.Point(22, 54);
            this.lblCustomerFN.Name = "lblCustomerFN";
            this.lblCustomerFN.Size = new System.Drawing.Size(89, 18);
            this.lblCustomerFN.TabIndex = 10;
            this.lblCustomerFN.Text = "First Name:";
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomer.Location = new System.Drawing.Point(173, 305);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(130, 29);
            this.btnAddCustomer.TabIndex = 9;
            this.btnAddCustomer.Text = "Add Customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click_1);
            // 
            // btnCustCancel
            // 
            this.btnCustCancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustCancel.Location = new System.Drawing.Point(6, 305);
            this.btnCustCancel.Name = "btnCustCancel";
            this.btnCustCancel.Size = new System.Drawing.Size(130, 29);
            this.btnCustCancel.TabIndex = 31;
            this.btnCustCancel.Text = "Cancel";
            this.btnCustCancel.UseVisualStyleBackColor = true;
            this.btnCustCancel.Click += new System.EventHandler(this.btnCustCancel_Click);
            // 
            // frmAddCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.grbNewCustomer);
            this.Name = "frmAddCustomer";
            this.Text = "AddCustomer";
            this.grbNewCustomer.ResumeLayout(false);
            this.grbNewCustomer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbNewCustomer;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCustomerCoupon;
        private System.Windows.Forms.TextBox TxtCustomerEmail;
        private System.Windows.Forms.TextBox txtCustomerPhone;
        private System.Windows.Forms.TextBox txtCustomerAddress;
        private System.Windows.Forms.TextBox txtCustomerLastName;
        private System.Windows.Forms.TextBox txtCustomerFirstName;
        private System.Windows.Forms.Label lblCoupon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerLN;
        private System.Windows.Forms.Label lblCustomerFN;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Button btnCustCancel;
    }
}