﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmAddNewCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbNewCustomer = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbCustomerIDCar = new System.Windows.Forms.ComboBox();
            this.txtCarId = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCarColor = new System.Windows.Forms.TextBox();
            this.txtCarModel = new System.Windows.Forms.TextBox();
            this.txtCarMake = new System.Windows.Forms.TextBox();
            this.txtCarRegNum = new System.Windows.Forms.TextBox();
            this.lblCoupon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerLN = new System.Windows.Forms.Label();
            this.lblCustomerFN = new System.Windows.Forms.Label();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.dtpNCTCar = new System.Windows.Forms.DateTimePicker();
            this.grbNewCustomer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbNewCustomer
            // 
            this.grbNewCustomer.Controls.Add(this.dtpNCTCar);
            this.grbNewCustomer.Controls.Add(this.btnCancel);
            this.grbNewCustomer.Controls.Add(this.cmbCustomerIDCar);
            this.grbNewCustomer.Controls.Add(this.txtCarId);
            this.grbNewCustomer.Controls.Add(this.label14);
            this.grbNewCustomer.Controls.Add(this.txtCarColor);
            this.grbNewCustomer.Controls.Add(this.txtCarModel);
            this.grbNewCustomer.Controls.Add(this.txtCarMake);
            this.grbNewCustomer.Controls.Add(this.txtCarRegNum);
            this.grbNewCustomer.Controls.Add(this.lblCoupon);
            this.grbNewCustomer.Controls.Add(this.lblEmail);
            this.grbNewCustomer.Controls.Add(this.lblPhone);
            this.grbNewCustomer.Controls.Add(this.lblAddress);
            this.grbNewCustomer.Controls.Add(this.lblCustomerLN);
            this.grbNewCustomer.Controls.Add(this.lblCustomerFN);
            this.grbNewCustomer.Controls.Add(this.btnAddCustomer);
            this.grbNewCustomer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNewCustomer.Location = new System.Drawing.Point(22, 5);
            this.grbNewCustomer.Name = "grbNewCustomer";
            this.grbNewCustomer.Size = new System.Drawing.Size(340, 351);
            this.grbNewCustomer.TabIndex = 18;
            this.grbNewCustomer.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(17, 305);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 29);
            this.btnCancel.TabIndex = 52;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cmbCustomerIDCar
            // 
            this.cmbCustomerIDCar.FormattingEnabled = true;
            this.cmbCustomerIDCar.Location = new System.Drawing.Point(132, 260);
            this.cmbCustomerIDCar.Name = "cmbCustomerIDCar";
            this.cmbCustomerIDCar.Size = new System.Drawing.Size(171, 26);
            this.cmbCustomerIDCar.TabIndex = 32;
            // 
            // txtCarId
            // 
            this.txtCarId.Location = new System.Drawing.Point(132, 9);
            this.txtCarId.Name = "txtCarId";
            this.txtCarId.Size = new System.Drawing.Size(171, 26);
            this.txtCarId.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "ID:";
            // 
            // txtCarColor
            // 
            this.txtCarColor.Location = new System.Drawing.Point(132, 177);
            this.txtCarColor.Name = "txtCarColor";
            this.txtCarColor.Size = new System.Drawing.Size(171, 26);
            this.txtCarColor.TabIndex = 23;
            // 
            // txtCarModel
            // 
            this.txtCarModel.Location = new System.Drawing.Point(132, 135);
            this.txtCarModel.Name = "txtCarModel";
            this.txtCarModel.Size = new System.Drawing.Size(171, 26);
            this.txtCarModel.TabIndex = 22;
            // 
            // txtCarMake
            // 
            this.txtCarMake.Location = new System.Drawing.Point(132, 93);
            this.txtCarMake.Name = "txtCarMake";
            this.txtCarMake.Size = new System.Drawing.Size(171, 26);
            this.txtCarMake.TabIndex = 21;
            // 
            // txtCarRegNum
            // 
            this.txtCarRegNum.Location = new System.Drawing.Point(132, 51);
            this.txtCarRegNum.Name = "txtCarRegNum";
            this.txtCarRegNum.Size = new System.Drawing.Size(171, 26);
            this.txtCarRegNum.TabIndex = 20;
            // 
            // lblCoupon
            // 
            this.lblCoupon.AutoSize = true;
            this.lblCoupon.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoupon.Location = new System.Drawing.Point(22, 263);
            this.lblCoupon.Name = "lblCoupon";
            this.lblCoupon.Size = new System.Drawing.Size(99, 18);
            this.lblCoupon.TabIndex = 19;
            this.lblCoupon.Text = "Customer ID:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(22, 221);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(77, 18);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "NCT Due:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(22, 180);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(50, 18);
            this.lblPhone.TabIndex = 17;
            this.lblPhone.Text = "Color:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(22, 138);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(55, 18);
            this.lblAddress.TabIndex = 12;
            this.lblAddress.Text = "Model:";
            // 
            // lblCustomerLN
            // 
            this.lblCustomerLN.AutoSize = true;
            this.lblCustomerLN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerLN.Location = new System.Drawing.Point(22, 96);
            this.lblCustomerLN.Name = "lblCustomerLN";
            this.lblCustomerLN.Size = new System.Drawing.Size(51, 18);
            this.lblCustomerLN.TabIndex = 11;
            this.lblCustomerLN.Text = "Make:";
            // 
            // lblCustomerFN
            // 
            this.lblCustomerFN.AutoSize = true;
            this.lblCustomerFN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFN.Location = new System.Drawing.Point(22, 54);
            this.lblCustomerFN.Name = "lblCustomerFN";
            this.lblCustomerFN.Size = new System.Drawing.Size(77, 18);
            this.lblCustomerFN.TabIndex = 10;
            this.lblCustomerFN.Text = "Reg Num:";
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomer.Location = new System.Drawing.Point(173, 305);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(130, 29);
            this.btnAddCustomer.TabIndex = 9;
            this.btnAddCustomer.Text = "Add Car";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // dtpNCTCar
            // 
            this.dtpNCTCar.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNCTCar.Location = new System.Drawing.Point(132, 221);
            this.dtpNCTCar.Name = "dtpNCTCar";
            this.dtpNCTCar.Size = new System.Drawing.Size(171, 26);
            this.dtpNCTCar.TabIndex = 53;
            // 
            // frmAddNewCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.grbNewCustomer);
            this.Name = "frmAddNewCar";
            this.Text = "AddNewCar";
            this.Load += new System.EventHandler(this.frmAddNewCar_Load);
            this.grbNewCustomer.ResumeLayout(false);
            this.grbNewCustomer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbNewCustomer;
        private System.Windows.Forms.TextBox txtCarId;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCarColor;
        private System.Windows.Forms.TextBox txtCarModel;
        private System.Windows.Forms.TextBox txtCarMake;
        private System.Windows.Forms.TextBox txtCarRegNum;
        private System.Windows.Forms.Label lblCoupon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerLN;
        private System.Windows.Forms.Label lblCustomerFN;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.ComboBox cmbCustomerIDCar;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DateTimePicker dtpNCTCar;
    }
}