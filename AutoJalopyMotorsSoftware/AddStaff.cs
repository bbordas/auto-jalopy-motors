﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmAddStaff : Form
    {
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();

        public frmAddStaff()
        {
            InitializeComponent();
        }


        private void frmAddStaff_Load(object sender, EventArgs e)
        {
            this.cmbAccess.DataSource = AutoJDC.tbStaffs.Select(a => a.AccessLevel)
                                                        .Distinct()
                                                        .Select(ac => ac);

        }

        private void btnAddStaff_Click(object sender, EventArgs e)
        {
            try
            {
                using (AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext())
                {
                    tbStaff staff = new tbStaff
                    {

                        StaffID = int.Parse(txtStaffID.Text),
                        StaffFirstName = txtStaffFirstName.Text,
                        StaffLastName = txtStaffLastName.Text,
                        StaffAddress = txtStaffAddress.Text,
                        StaffPhone = txtStaffPhone.Text,
                        StaffEmail = TxtStaffEmail.Text,
                        AccessLevel = cmbAccess.SelectedItem.ToString(),
                        StaffPassword = txtStaffPassword.Text,
                    };

                    AutoJDC.tbStaffs.InsertOnSubmit(staff);
                    AutoJDC.SubmitChanges();
                    DialogResult = DialogResult.OK;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = DialogResult.Cancel;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
