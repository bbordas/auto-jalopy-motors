﻿namespace AutoJalopyMotorsSoftware
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabChangePass = new System.Windows.Forms.TabPage();
            this.login = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtConfirmPass = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.tabJobCardsAdm = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.dgvShow = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnaddNewJobCard = new System.Windows.Forms.Button();
            this.tabCars = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAddCar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvCustDetails = new System.Windows.Forms.DataGridView();
            this.lblcar = new System.Windows.Forms.Label();
            this.btnUpdateCarDetails = new System.Windows.Forms.Button();
            this.dgvCars = new System.Windows.Forms.DataGridView();
            this.btnUpdateCar = new System.Windows.Forms.Button();
            this.tbCustomer = new System.Windows.Forms.TabPage();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.btnUpdateCustomer = new System.Windows.Forms.Button();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.tbReport = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.btnReportWorkLoad = new System.Windows.Forms.Button();
            this.btnReportJobHistory = new System.Windows.Forms.Button();
            this.btnReportRangeOfVehicles = new System.Windows.Forms.Button();
            this.dtpEndReport = new System.Windows.Forms.DateTimePicker();
            this.dtpStartReport = new System.Windows.Forms.DateTimePicker();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnStaffWorkLoad = new System.Windows.Forms.Button();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.btnUpdateStaff = new System.Windows.Forms.Button();
            this.dgvStaff = new System.Windows.Forms.DataGridView();
            this.tabCarsStaff = new System.Windows.Forms.TabControl();
            this.btnLogOut2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvJobCards = new System.Windows.Forms.DataGridView();
            this.tabChangePass.SuspendLayout();
            this.login.SuspendLayout();
            this.tabJobCardsAdm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShow)).BeginInit();
            this.tabCars.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCars)).BeginInit();
            this.tbCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.tbReport.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaff)).BeginInit();
            this.tabCarsStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobCards)).BeginInit();
            this.SuspendLayout();
            // 
            // tabChangePass
            // 
            this.tabChangePass.Controls.Add(this.login);
            this.tabChangePass.Location = new System.Drawing.Point(4, 25);
            this.tabChangePass.Name = "tabChangePass";
            this.tabChangePass.Padding = new System.Windows.Forms.Padding(3);
            this.tabChangePass.Size = new System.Drawing.Size(792, 571);
            this.tabChangePass.TabIndex = 5;
            this.tabChangePass.Text = "Change Password";
            this.tabChangePass.UseVisualStyleBackColor = true;
            // 
            // login
            // 
            this.login.Controls.Add(this.label6);
            this.login.Controls.Add(this.txtUserName);
            this.login.Controls.Add(this.label29);
            this.login.Controls.Add(this.txtConfirmPass);
            this.login.Controls.Add(this.label26);
            this.login.Controls.Add(this.btnChangePass);
            this.login.Controls.Add(this.lblPassword);
            this.login.Controls.Add(this.txtNewPass);
            this.login.Controls.Add(this.lblID);
            this.login.Controls.Add(this.txtOldPass);
            this.login.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.Location = new System.Drawing.Point(196, 55);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(384, 394);
            this.login.TabIndex = 28;
            this.login.TabStop = false;
            this.login.Text = "Change Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(64, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 18);
            this.label6.TabIndex = 35;
            this.label6.Text = "UserName";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(180, 134);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(158, 26);
            this.txtUserName.TabIndex = 34;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(34, 264);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 18);
            this.label29.TabIndex = 33;
            this.label29.Text = "Confirm Password:";
            // 
            // txtConfirmPass
            // 
            this.txtConfirmPass.Location = new System.Drawing.Point(180, 264);
            this.txtConfirmPass.Name = "txtConfirmPass";
            this.txtConfirmPass.Size = new System.Drawing.Size(158, 26);
            this.txtConfirmPass.TabIndex = 32;
            this.txtConfirmPass.UseSystemPasswordChar = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(16, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(157, 19);
            this.label26.TabIndex = 31;
            this.label26.Text = "Auto Jalopy Motors";
            // 
            // btnChangePass
            // 
            this.btnChangePass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePass.Location = new System.Drawing.Point(180, 335);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(158, 31);
            this.btnChangePass.TabIndex = 30;
            this.btnChangePass.Text = "Change";
            this.btnChangePass.UseVisualStyleBackColor = true;
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(56, 219);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(117, 18);
            this.lblPassword.TabIndex = 28;
            this.lblPassword.Text = "New Password:";
            // 
            // txtNewPass
            // 
            this.txtNewPass.Location = new System.Drawing.Point(180, 219);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.Size = new System.Drawing.Size(158, 26);
            this.txtNewPass.TabIndex = 27;
            this.txtNewPass.UseSystemPasswordChar = true;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(64, 176);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(106, 18);
            this.lblID.TabIndex = 26;
            this.lblID.Text = "Old Password";
            // 
            // txtOldPass
            // 
            this.txtOldPass.Location = new System.Drawing.Point(180, 176);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.Size = new System.Drawing.Size(158, 26);
            this.txtOldPass.TabIndex = 25;
            // 
            // tabJobCardsAdm
            // 
            this.tabJobCardsAdm.Controls.Add(this.dgvJobCards);
            this.tabJobCardsAdm.Controls.Add(this.button2);
            this.tabJobCardsAdm.Controls.Add(this.dgvShow);
            this.tabJobCardsAdm.Controls.Add(this.btnDelete);
            this.tabJobCardsAdm.Controls.Add(this.btnaddNewJobCard);
            this.tabJobCardsAdm.Location = new System.Drawing.Point(4, 25);
            this.tabJobCardsAdm.Name = "tabJobCardsAdm";
            this.tabJobCardsAdm.Padding = new System.Windows.Forms.Padding(3);
            this.tabJobCardsAdm.Size = new System.Drawing.Size(792, 571);
            this.tabJobCardsAdm.TabIndex = 2;
            this.tabJobCardsAdm.Text = "Job Cards";
            this.tabJobCardsAdm.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(506, 426);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(177, 29);
            this.button2.TabIndex = 58;
            this.button2.Text = "Show Cards";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dgvShow
            // 
            this.dgvShow.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvShow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShow.Location = new System.Drawing.Point(214, 25);
            this.dgvShow.Name = "dgvShow";
            this.dgvShow.Size = new System.Drawing.Size(526, 384);
            this.dgvShow.TabIndex = 57;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(266, 426);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(177, 29);
            this.btnDelete.TabIndex = 56;
            this.btnDelete.Text = "Delete Card";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnaddNewJobCard
            // 
            this.btnaddNewJobCard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddNewJobCard.Location = new System.Drawing.Point(17, 426);
            this.btnaddNewJobCard.Name = "btnaddNewJobCard";
            this.btnaddNewJobCard.Size = new System.Drawing.Size(177, 29);
            this.btnaddNewJobCard.TabIndex = 50;
            this.btnaddNewJobCard.Text = "Add New Card";
            this.btnaddNewJobCard.UseVisualStyleBackColor = true;
            this.btnaddNewJobCard.Click += new System.EventHandler(this.btnaddNewJobCard_Click);
            // 
            // tabCars
            // 
            this.tabCars.Controls.Add(this.groupBox2);
            this.tabCars.Location = new System.Drawing.Point(4, 25);
            this.tabCars.Name = "tabCars";
            this.tabCars.Padding = new System.Windows.Forms.Padding(3);
            this.tabCars.Size = new System.Drawing.Size(792, 571);
            this.tabCars.TabIndex = 1;
            this.tabCars.Text = "Cars";
            this.tabCars.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAddCar);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dgvCustDetails);
            this.groupBox2.Controls.Add(this.lblcar);
            this.groupBox2.Controls.Add(this.btnUpdateCarDetails);
            this.groupBox2.Controls.Add(this.dgvCars);
            this.groupBox2.Controls.Add(this.btnUpdateCar);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(823, 481);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            // 
            // btnAddCar
            // 
            this.btnAddCar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCar.Location = new System.Drawing.Point(461, 419);
            this.btnAddCar.Name = "btnAddCar";
            this.btnAddCar.Size = new System.Drawing.Size(127, 29);
            this.btnAddCar.TabIndex = 52;
            this.btnAddCar.Text = "Add Car";
            this.btnAddCar.UseVisualStyleBackColor = true;
            this.btnAddCar.Click += new System.EventHandler(this.btnAddCar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 408);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 18);
            this.label4.TabIndex = 50;
            this.label4.Text = "Cars";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 49;
            this.label1.Text = "Owner Details";
            // 
            // dgvCustDetails
            // 
            this.dgvCustDetails.AllowDrop = true;
            this.dgvCustDetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCustDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCustDetails.Location = new System.Drawing.Point(19, 24);
            this.dgvCustDetails.Name = "dgvCustDetails";
            this.dgvCustDetails.Size = new System.Drawing.Size(443, 87);
            this.dgvCustDetails.TabIndex = 48;
            // 
            // lblcar
            // 
            this.lblcar.AutoSize = true;
            this.lblcar.Location = new System.Drawing.Point(436, 146);
            this.lblcar.Name = "lblcar";
            this.lblcar.Size = new System.Drawing.Size(311, 18);
            this.lblcar.TabIndex = 47;
            this.lblcar.Text = "Click on a car record to get its owner details";
            // 
            // btnUpdateCarDetails
            // 
            this.btnUpdateCarDetails.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCarDetails.Location = new System.Drawing.Point(623, 419);
            this.btnUpdateCarDetails.Name = "btnUpdateCarDetails";
            this.btnUpdateCarDetails.Size = new System.Drawing.Size(127, 29);
            this.btnUpdateCarDetails.TabIndex = 42;
            this.btnUpdateCarDetails.Text = "Update";
            this.btnUpdateCarDetails.UseVisualStyleBackColor = true;
            this.btnUpdateCarDetails.Click += new System.EventHandler(this.btnUpdateCarDetails_Click);
            // 
            // dgvCars
            // 
            this.dgvCars.AllowDrop = true;
            this.dgvCars.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCars.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCars.Location = new System.Drawing.Point(14, 167);
            this.dgvCars.Name = "dgvCars";
            this.dgvCars.Size = new System.Drawing.Size(743, 237);
            this.dgvCars.TabIndex = 37;
            this.dgvCars.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCars_CellContentClick);
            // 
            // btnUpdateCar
            // 
            this.btnUpdateCar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCar.Location = new System.Drawing.Point(3, 498);
            this.btnUpdateCar.Name = "btnUpdateCar";
            this.btnUpdateCar.Size = new System.Drawing.Size(130, 29);
            this.btnUpdateCar.TabIndex = 9;
            this.btnUpdateCar.Text = "Update Details";
            this.btnUpdateCar.UseVisualStyleBackColor = true;
            // 
            // tbCustomer
            // 
            this.tbCustomer.Controls.Add(this.btnAddCustomer);
            this.tbCustomer.Controls.Add(this.btnUpdateCustomer);
            this.tbCustomer.Controls.Add(this.dgvCustomer);
            this.tbCustomer.Location = new System.Drawing.Point(4, 25);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tbCustomer.Size = new System.Drawing.Size(792, 571);
            this.tbCustomer.TabIndex = 6;
            this.tbCustomer.Text = "Customer";
            this.tbCustomer.UseVisualStyleBackColor = true;
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomer.Location = new System.Drawing.Point(470, 456);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(127, 29);
            this.btnAddCustomer.TabIndex = 54;
            this.btnAddCustomer.Text = "Add Customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // btnUpdateCustomer
            // 
            this.btnUpdateCustomer.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCustomer.Location = new System.Drawing.Point(632, 456);
            this.btnUpdateCustomer.Name = "btnUpdateCustomer";
            this.btnUpdateCustomer.Size = new System.Drawing.Size(127, 29);
            this.btnUpdateCustomer.TabIndex = 53;
            this.btnUpdateCustomer.Text = "Update";
            this.btnUpdateCustomer.UseVisualStyleBackColor = true;
            this.btnUpdateCustomer.Click += new System.EventHandler(this.btnUpdateCustomer_Click);
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AllowDrop = true;
            this.dgvCustomer.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCustomer.Location = new System.Drawing.Point(16, 16);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.Size = new System.Drawing.Size(743, 415);
            this.dgvCustomer.TabIndex = 38;
            // 
            // tbReport
            // 
            this.tbReport.Controls.Add(this.groupBox5);
            this.tbReport.Location = new System.Drawing.Point(4, 25);
            this.tbReport.Name = "tbReport";
            this.tbReport.Size = new System.Drawing.Size(792, 571);
            this.tbReport.TabIndex = 9;
            this.tbReport.Text = "Report";
            this.tbReport.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.btnReportWorkLoad);
            this.groupBox5.Controls.Add(this.btnReportJobHistory);
            this.groupBox5.Controls.Add(this.btnReportRangeOfVehicles);
            this.groupBox5.Controls.Add(this.dtpEndReport);
            this.groupBox5.Controls.Add(this.dtpStartReport);
            this.groupBox5.Controls.Add(this.dgvReport);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(774, 529);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "View Reports";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(504, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(237, 29);
            this.button1.TabIndex = 58;
            this.button1.Text = "Range of Vehicles";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(616, 176);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 18);
            this.label20.TabIndex = 57;
            this.label20.Text = "-";
            // 
            // btnReportWorkLoad
            // 
            this.btnReportWorkLoad.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportWorkLoad.Location = new System.Drawing.Point(504, 228);
            this.btnReportWorkLoad.Name = "btnReportWorkLoad";
            this.btnReportWorkLoad.Size = new System.Drawing.Size(237, 29);
            this.btnReportWorkLoad.TabIndex = 9;
            this.btnReportWorkLoad.Text = "WorkLoad";
            this.btnReportWorkLoad.UseVisualStyleBackColor = true;
            this.btnReportWorkLoad.Click += new System.EventHandler(this.btnReportWorkLoad_Click);
            // 
            // btnReportJobHistory
            // 
            this.btnReportJobHistory.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportJobHistory.Location = new System.Drawing.Point(504, 277);
            this.btnReportJobHistory.Name = "btnReportJobHistory";
            this.btnReportJobHistory.Size = new System.Drawing.Size(237, 29);
            this.btnReportJobHistory.TabIndex = 12;
            this.btnReportJobHistory.Text = "Job History";
            this.btnReportJobHistory.UseVisualStyleBackColor = true;
            this.btnReportJobHistory.Click += new System.EventHandler(this.btnReportJobHistory_Click);
            // 
            // btnReportRangeOfVehicles
            // 
            this.btnReportRangeOfVehicles.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReportRangeOfVehicles.Location = new System.Drawing.Point(773, 458);
            this.btnReportRangeOfVehicles.Name = "btnReportRangeOfVehicles";
            this.btnReportRangeOfVehicles.Size = new System.Drawing.Size(149, 29);
            this.btnReportRangeOfVehicles.TabIndex = 11;
            this.btnReportRangeOfVehicles.Text = "Range of Vehicles";
            this.btnReportRangeOfVehicles.UseVisualStyleBackColor = true;
            // 
            // dtpEndReport
            // 
            this.dtpEndReport.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEndReport.Location = new System.Drawing.Point(635, 175);
            this.dtpEndReport.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpEndReport.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpEndReport.Name = "dtpEndReport";
            this.dtpEndReport.Size = new System.Drawing.Size(106, 26);
            this.dtpEndReport.TabIndex = 55;
            // 
            // dtpStartReport
            // 
            this.dtpStartReport.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpStartReport.Location = new System.Drawing.Point(504, 175);
            this.dtpStartReport.MaxDate = new System.DateTime(2100, 1, 1, 0, 0, 0, 0);
            this.dtpStartReport.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpStartReport.Name = "dtpStartReport";
            this.dtpStartReport.Size = new System.Drawing.Size(106, 26);
            this.dtpStartReport.TabIndex = 54;
            // 
            // dgvReport
            // 
            this.dgvReport.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.Location = new System.Drawing.Point(22, 38);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.Size = new System.Drawing.Size(451, 415);
            this.dgvReport.TabIndex = 10;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnStaffWorkLoad);
            this.tabPage2.Controls.Add(this.btnAddStaff);
            this.tabPage2.Controls.Add(this.btnUpdateStaff);
            this.tabPage2.Controls.Add(this.dgvStaff);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(792, 571);
            this.tabPage2.TabIndex = 7;
            this.tabPage2.Text = "Staff";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnStaffWorkLoad
            // 
            this.btnStaffWorkLoad.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStaffWorkLoad.Location = new System.Drawing.Point(320, 460);
            this.btnStaffWorkLoad.Name = "btnStaffWorkLoad";
            this.btnStaffWorkLoad.Size = new System.Drawing.Size(127, 29);
            this.btnStaffWorkLoad.TabIndex = 58;
            this.btnStaffWorkLoad.Text = " WorkLoad";
            this.btnStaffWorkLoad.UseVisualStyleBackColor = true;
            this.btnStaffWorkLoad.Click += new System.EventHandler(this.btnStaffWorkLoad_Click);
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStaff.Location = new System.Drawing.Point(471, 460);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Size = new System.Drawing.Size(127, 29);
            this.btnAddStaff.TabIndex = 57;
            this.btnAddStaff.Text = "AddStaff";
            this.btnAddStaff.UseVisualStyleBackColor = true;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // btnUpdateStaff
            // 
            this.btnUpdateStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateStaff.Location = new System.Drawing.Point(633, 460);
            this.btnUpdateStaff.Name = "btnUpdateStaff";
            this.btnUpdateStaff.Size = new System.Drawing.Size(127, 29);
            this.btnUpdateStaff.TabIndex = 56;
            this.btnUpdateStaff.Text = "Update Staff";
            this.btnUpdateStaff.UseVisualStyleBackColor = true;
            this.btnUpdateStaff.Click += new System.EventHandler(this.btnUpdateStaff_Click);
            // 
            // dgvStaff
            // 
            this.dgvStaff.AllowDrop = true;
            this.dgvStaff.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStaff.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvStaff.Location = new System.Drawing.Point(17, 20);
            this.dgvStaff.Name = "dgvStaff";
            this.dgvStaff.Size = new System.Drawing.Size(743, 415);
            this.dgvStaff.TabIndex = 55;
            this.dgvStaff.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStaff_RowLeave_1);
            // 
            // tabCarsStaff
            // 
            this.tabCarsStaff.Controls.Add(this.tabPage2);
            this.tabCarsStaff.Controls.Add(this.tbCustomer);
            this.tabCarsStaff.Controls.Add(this.tabCars);
            this.tabCarsStaff.Controls.Add(this.tabJobCardsAdm);
            this.tabCarsStaff.Controls.Add(this.tbReport);
            this.tabCarsStaff.Controls.Add(this.tabChangePass);
            this.tabCarsStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCarsStaff.Location = new System.Drawing.Point(0, 42);
            this.tabCarsStaff.Name = "tabCarsStaff";
            this.tabCarsStaff.SelectedIndex = 0;
            this.tabCarsStaff.Size = new System.Drawing.Size(800, 600);
            this.tabCarsStaff.TabIndex = 51;
            // 
            // btnLogOut2
            // 
            this.btnLogOut2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut2.Location = new System.Drawing.Point(642, 12);
            this.btnLogOut2.Name = "btnLogOut2";
            this.btnLogOut2.Size = new System.Drawing.Size(130, 30);
            this.btnLogOut2.TabIndex = 55;
            this.btnLogOut2.Text = "Log Out";
            this.btnLogOut2.UseVisualStyleBackColor = true;
            this.btnLogOut2.Click += new System.EventHandler(this.btnLogOut2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(286, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 19);
            this.label2.TabIndex = 54;
            this.label2.Text = "Auto Jalopy Motors: Admin";
            // 
            // dgvJobCards
            // 
            this.dgvJobCards.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvJobCards.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobCards.Location = new System.Drawing.Point(17, 25);
            this.dgvJobCards.Name = "dgvJobCards";
            this.dgvJobCards.Size = new System.Drawing.Size(177, 384);
            this.dgvJobCards.TabIndex = 59;
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnLogOut2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabCarsStaff);
            this.Name = "Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Admin_Load);
            this.tabChangePass.ResumeLayout(false);
            this.login.ResumeLayout(false);
            this.login.PerformLayout();
            this.tabJobCardsAdm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShow)).EndInit();
            this.tabCars.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCars)).EndInit();
            this.tbCustomer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.tbReport.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaff)).EndInit();
            this.tabCarsStaff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobCards)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabChangePass;
        private System.Windows.Forms.GroupBox login;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtConfirmPass;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.TabPage tabJobCardsAdm;
        private System.Windows.Forms.Button btnaddNewJobCard;
        private System.Windows.Forms.TabPage tabCars;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAddCar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvCustDetails;
        private System.Windows.Forms.Label lblcar;
        private System.Windows.Forms.Button btnUpdateCarDetails;
        private System.Windows.Forms.DataGridView dgvCars;
        private System.Windows.Forms.Button btnUpdateCar;
        private System.Windows.Forms.TabPage tbCustomer;
        private System.Windows.Forms.TabPage tbReport;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabCarsStaff;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnReportWorkLoad;
        private System.Windows.Forms.Button btnReportJobHistory;
        private System.Windows.Forms.Button btnReportRangeOfVehicles;
        private System.Windows.Forms.DateTimePicker dtpEndReport;
        private System.Windows.Forms.DateTimePicker dtpStartReport;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Button btnUpdateCustomer;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.Button btnUpdateStaff;
        private System.Windows.Forms.DataGridView dgvStaff;
        private System.Windows.Forms.Button btnStaffWorkLoad;
        private System.Windows.Forms.Button btnLogOut2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvShow;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dgvJobCards;
    }
}