﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmAddStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbNewCustomer = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbAccess = new System.Windows.Forms.ComboBox();
            this.txtStaffID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtStaffPassword = new System.Windows.Forms.TextBox();
            this.TxtStaffEmail = new System.Windows.Forms.TextBox();
            this.txtStaffPhone = new System.Windows.Forms.TextBox();
            this.txtStaffAddress = new System.Windows.Forms.TextBox();
            this.txtStaffLastName = new System.Windows.Forms.TextBox();
            this.txtStaffFirstName = new System.Windows.Forms.TextBox();
            this.lblCoupon = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCustomerLN = new System.Windows.Forms.Label();
            this.lblCustomerFN = new System.Windows.Forms.Label();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.grbNewCustomer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbNewCustomer
            // 
            this.grbNewCustomer.Controls.Add(this.btnCancel);
            this.grbNewCustomer.Controls.Add(this.label1);
            this.grbNewCustomer.Controls.Add(this.cmbAccess);
            this.grbNewCustomer.Controls.Add(this.txtStaffID);
            this.grbNewCustomer.Controls.Add(this.label14);
            this.grbNewCustomer.Controls.Add(this.txtStaffPassword);
            this.grbNewCustomer.Controls.Add(this.TxtStaffEmail);
            this.grbNewCustomer.Controls.Add(this.txtStaffPhone);
            this.grbNewCustomer.Controls.Add(this.txtStaffAddress);
            this.grbNewCustomer.Controls.Add(this.txtStaffLastName);
            this.grbNewCustomer.Controls.Add(this.txtStaffFirstName);
            this.grbNewCustomer.Controls.Add(this.lblCoupon);
            this.grbNewCustomer.Controls.Add(this.lblEmail);
            this.grbNewCustomer.Controls.Add(this.lblPhone);
            this.grbNewCustomer.Controls.Add(this.lblAddress);
            this.grbNewCustomer.Controls.Add(this.lblCustomerLN);
            this.grbNewCustomer.Controls.Add(this.lblCustomerFN);
            this.grbNewCustomer.Controls.Add(this.btnAddStaff);
            this.grbNewCustomer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNewCustomer.Location = new System.Drawing.Point(20, 13);
            this.grbNewCustomer.Name = "grbNewCustomer";
            this.grbNewCustomer.Size = new System.Drawing.Size(340, 399);
            this.grbNewCustomer.TabIndex = 18;
            this.grbNewCustomer.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(25, 354);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 29);
            this.btnCancel.TabIndex = 53;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 303);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 18);
            this.label1.TabIndex = 32;
            this.label1.Text = "Role:";
            // 
            // cmbAccess
            // 
            this.cmbAccess.FormattingEnabled = true;
            this.cmbAccess.Location = new System.Drawing.Point(132, 300);
            this.cmbAccess.Name = "cmbAccess";
            this.cmbAccess.Size = new System.Drawing.Size(171, 26);
            this.cmbAccess.TabIndex = 31;
            // 
            // txtStaffID
            // 
            this.txtStaffID.Location = new System.Drawing.Point(132, 9);
            this.txtStaffID.Name = "txtStaffID";
            this.txtStaffID.Size = new System.Drawing.Size(171, 26);
            this.txtStaffID.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(22, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(27, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "ID:";
            // 
            // txtStaffPassword
            // 
            this.txtStaffPassword.Location = new System.Drawing.Point(132, 260);
            this.txtStaffPassword.Name = "txtStaffPassword";
            this.txtStaffPassword.Size = new System.Drawing.Size(171, 26);
            this.txtStaffPassword.TabIndex = 25;
            // 
            // TxtStaffEmail
            // 
            this.TxtStaffEmail.Location = new System.Drawing.Point(132, 218);
            this.TxtStaffEmail.Name = "TxtStaffEmail";
            this.TxtStaffEmail.Size = new System.Drawing.Size(171, 26);
            this.TxtStaffEmail.TabIndex = 24;
            // 
            // txtStaffPhone
            // 
            this.txtStaffPhone.Location = new System.Drawing.Point(132, 177);
            this.txtStaffPhone.Name = "txtStaffPhone";
            this.txtStaffPhone.Size = new System.Drawing.Size(171, 26);
            this.txtStaffPhone.TabIndex = 23;
            // 
            // txtStaffAddress
            // 
            this.txtStaffAddress.Location = new System.Drawing.Point(132, 135);
            this.txtStaffAddress.Name = "txtStaffAddress";
            this.txtStaffAddress.Size = new System.Drawing.Size(171, 26);
            this.txtStaffAddress.TabIndex = 22;
            // 
            // txtStaffLastName
            // 
            this.txtStaffLastName.Location = new System.Drawing.Point(132, 93);
            this.txtStaffLastName.Name = "txtStaffLastName";
            this.txtStaffLastName.Size = new System.Drawing.Size(171, 26);
            this.txtStaffLastName.TabIndex = 21;
            // 
            // txtStaffFirstName
            // 
            this.txtStaffFirstName.Location = new System.Drawing.Point(132, 51);
            this.txtStaffFirstName.Name = "txtStaffFirstName";
            this.txtStaffFirstName.Size = new System.Drawing.Size(171, 26);
            this.txtStaffFirstName.TabIndex = 20;
            // 
            // lblCoupon
            // 
            this.lblCoupon.AutoSize = true;
            this.lblCoupon.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoupon.Location = new System.Drawing.Point(22, 263);
            this.lblCoupon.Name = "lblCoupon";
            this.lblCoupon.Size = new System.Drawing.Size(78, 18);
            this.lblCoupon.TabIndex = 19;
            this.lblCoupon.Text = "Password";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(22, 221);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(52, 18);
            this.lblEmail.TabIndex = 18;
            this.lblEmail.Text = "Email:";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhone.Location = new System.Drawing.Point(22, 180);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(57, 18);
            this.lblPhone.TabIndex = 17;
            this.lblPhone.Text = "Phone:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(22, 138);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(71, 18);
            this.lblAddress.TabIndex = 12;
            this.lblAddress.Text = "Address:";
            // 
            // lblCustomerLN
            // 
            this.lblCustomerLN.AutoSize = true;
            this.lblCustomerLN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerLN.Location = new System.Drawing.Point(22, 96);
            this.lblCustomerLN.Name = "lblCustomerLN";
            this.lblCustomerLN.Size = new System.Drawing.Size(81, 18);
            this.lblCustomerLN.TabIndex = 11;
            this.lblCustomerLN.Text = "Lastname:";
            // 
            // lblCustomerFN
            // 
            this.lblCustomerFN.AutoSize = true;
            this.lblCustomerFN.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFN.Location = new System.Drawing.Point(22, 54);
            this.lblCustomerFN.Name = "lblCustomerFN";
            this.lblCustomerFN.Size = new System.Drawing.Size(89, 18);
            this.lblCustomerFN.TabIndex = 10;
            this.lblCustomerFN.Text = "First Name:";
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStaff.Location = new System.Drawing.Point(200, 354);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Size = new System.Drawing.Size(130, 29);
            this.btnAddStaff.TabIndex = 9;
            this.btnAddStaff.Text = "Add Staff";
            this.btnAddStaff.UseVisualStyleBackColor = true;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // frmAddStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 418);
            this.Controls.Add(this.grbNewCustomer);
            this.Name = "frmAddStaff";
            this.Text = "AddStaff";
            this.Load += new System.EventHandler(this.frmAddStaff_Load);
            this.grbNewCustomer.ResumeLayout(false);
            this.grbNewCustomer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbNewCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbAccess;
        private System.Windows.Forms.TextBox txtStaffID;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtStaffPassword;
        private System.Windows.Forms.TextBox TxtStaffEmail;
        private System.Windows.Forms.TextBox txtStaffPhone;
        private System.Windows.Forms.TextBox txtStaffAddress;
        private System.Windows.Forms.TextBox txtStaffLastName;
        private System.Windows.Forms.TextBox txtStaffFirstName;
        private System.Windows.Forms.Label lblCoupon;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblCustomerLN;
        private System.Windows.Forms.Label lblCustomerFN;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.Button btnCancel;
    }
}