﻿namespace AutoJalopyMotorsSoftware
{
    partial class frmStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabCarsStaff = new System.Windows.Forms.TabControl();
            this.tabCars = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvCustDetailsStaff = new System.Windows.Forms.DataGridView();
            this.lblcar = new System.Windows.Forms.Label();
            this.btnUpdateCarDetails = new System.Windows.Forms.Button();
            this.dgvCarsStaff = new System.Windows.Forms.DataGridView();
            this.btnUpdateCar = new System.Windows.Forms.Button();
            this.tabJobCardsAdm = new System.Windows.Forms.TabPage();
            this.btnShowCardStaff = new System.Windows.Forms.Button();
            this.dgvShowJobCardStaff = new System.Windows.Forms.DataGridView();
            this.btnaddNewJobCard = new System.Windows.Forms.Button();
            this.tabChangePass = new System.Windows.Forms.TabPage();
            this.login = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtConfirmPass = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtNewPass = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtOldPass = new System.Windows.Forms.TextBox();
            this.btnLogOut2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvJobCardsStaff = new System.Windows.Forms.DataGridView();
            this.tabCarsStaff.SuspendLayout();
            this.tabCars.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustDetailsStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarsStaff)).BeginInit();
            this.tabJobCardsAdm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowJobCardStaff)).BeginInit();
            this.tabChangePass.SuspendLayout();
            this.login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobCardsStaff)).BeginInit();
            this.SuspendLayout();
            // 
            // tabCarsStaff
            // 
            this.tabCarsStaff.Controls.Add(this.tabCars);
            this.tabCarsStaff.Controls.Add(this.tabJobCardsAdm);
            this.tabCarsStaff.Controls.Add(this.tabChangePass);
            this.tabCarsStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCarsStaff.Location = new System.Drawing.Point(0, 30);
            this.tabCarsStaff.Name = "tabCarsStaff";
            this.tabCarsStaff.SelectedIndex = 0;
            this.tabCarsStaff.Size = new System.Drawing.Size(784, 534);
            this.tabCarsStaff.TabIndex = 50;
            // 
            // tabCars
            // 
            this.tabCars.Controls.Add(this.groupBox2);
            this.tabCars.Location = new System.Drawing.Point(4, 25);
            this.tabCars.Name = "tabCars";
            this.tabCars.Padding = new System.Windows.Forms.Padding(3);
            this.tabCars.Size = new System.Drawing.Size(776, 505);
            this.tabCars.TabIndex = 1;
            this.tabCars.Text = "Cars";
            this.tabCars.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dgvCustDetailsStaff);
            this.groupBox2.Controls.Add(this.lblcar);
            this.groupBox2.Controls.Add(this.btnUpdateCarDetails);
            this.groupBox2.Controls.Add(this.dgvCarsStaff);
            this.groupBox2.Controls.Add(this.btnUpdateCar);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(823, 481);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 408);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 18);
            this.label4.TabIndex = 50;
            this.label4.Text = "Cars";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 18);
            this.label1.TabIndex = 49;
            this.label1.Text = "Owner Details";
            // 
            // dgvCustDetailsStaff
            // 
            this.dgvCustDetailsStaff.AllowDrop = true;
            this.dgvCustDetailsStaff.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCustDetailsStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustDetailsStaff.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCustDetailsStaff.Location = new System.Drawing.Point(19, 24);
            this.dgvCustDetailsStaff.Name = "dgvCustDetailsStaff";
            this.dgvCustDetailsStaff.Size = new System.Drawing.Size(443, 87);
            this.dgvCustDetailsStaff.TabIndex = 48;
            // 
            // lblcar
            // 
            this.lblcar.AutoSize = true;
            this.lblcar.Location = new System.Drawing.Point(436, 146);
            this.lblcar.Name = "lblcar";
            this.lblcar.Size = new System.Drawing.Size(311, 18);
            this.lblcar.TabIndex = 47;
            this.lblcar.Text = "Click on a car record to get its owner details";
            // 
            // btnUpdateCarDetails
            // 
            this.btnUpdateCarDetails.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCarDetails.Location = new System.Drawing.Point(623, 419);
            this.btnUpdateCarDetails.Name = "btnUpdateCarDetails";
            this.btnUpdateCarDetails.Size = new System.Drawing.Size(127, 29);
            this.btnUpdateCarDetails.TabIndex = 42;
            this.btnUpdateCarDetails.Text = "Update";
            this.btnUpdateCarDetails.UseVisualStyleBackColor = true;
            this.btnUpdateCarDetails.Click += new System.EventHandler(this.btnUpdateCarDetails_Click);
            // 
            // dgvCarsStaff
            // 
            this.dgvCarsStaff.AllowDrop = true;
            this.dgvCarsStaff.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCarsStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCarsStaff.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCarsStaff.Location = new System.Drawing.Point(14, 167);
            this.dgvCarsStaff.Name = "dgvCarsStaff";
            this.dgvCarsStaff.Size = new System.Drawing.Size(743, 237);
            this.dgvCarsStaff.TabIndex = 37;
            this.dgvCarsStaff.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCarsStaff_CellContentClick);
            // 
            // btnUpdateCar
            // 
            this.btnUpdateCar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateCar.Location = new System.Drawing.Point(3, 498);
            this.btnUpdateCar.Name = "btnUpdateCar";
            this.btnUpdateCar.Size = new System.Drawing.Size(130, 29);
            this.btnUpdateCar.TabIndex = 9;
            this.btnUpdateCar.Text = "Update Details";
            this.btnUpdateCar.UseVisualStyleBackColor = true;
            // 
            // tabJobCardsAdm
            // 
            this.tabJobCardsAdm.Controls.Add(this.dgvJobCardsStaff);
            this.tabJobCardsAdm.Controls.Add(this.btnShowCardStaff);
            this.tabJobCardsAdm.Controls.Add(this.dgvShowJobCardStaff);
            this.tabJobCardsAdm.Controls.Add(this.btnaddNewJobCard);
            this.tabJobCardsAdm.Location = new System.Drawing.Point(4, 25);
            this.tabJobCardsAdm.Name = "tabJobCardsAdm";
            this.tabJobCardsAdm.Padding = new System.Windows.Forms.Padding(3);
            this.tabJobCardsAdm.Size = new System.Drawing.Size(776, 505);
            this.tabJobCardsAdm.TabIndex = 2;
            this.tabJobCardsAdm.Text = "Job Cards";
            this.tabJobCardsAdm.UseVisualStyleBackColor = true;
            // 
            // btnShowCardStaff
            // 
            this.btnShowCardStaff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowCardStaff.Location = new System.Drawing.Point(213, 440);
            this.btnShowCardStaff.Name = "btnShowCardStaff";
            this.btnShowCardStaff.Size = new System.Drawing.Size(177, 29);
            this.btnShowCardStaff.TabIndex = 59;
            this.btnShowCardStaff.Text = "Show Cards";
            this.btnShowCardStaff.UseVisualStyleBackColor = true;
            this.btnShowCardStaff.Click += new System.EventHandler(this.btnShowCardStaff_Click);
            // 
            // dgvShowJobCardStaff
            // 
            this.dgvShowJobCardStaff.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvShowJobCardStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShowJobCardStaff.Location = new System.Drawing.Point(213, 25);
            this.dgvShowJobCardStaff.Name = "dgvShowJobCardStaff";
            this.dgvShowJobCardStaff.Size = new System.Drawing.Size(541, 401);
            this.dgvShowJobCardStaff.TabIndex = 58;
            // 
            // btnaddNewJobCard
            // 
            this.btnaddNewJobCard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddNewJobCard.Location = new System.Drawing.Point(17, 440);
            this.btnaddNewJobCard.Name = "btnaddNewJobCard";
            this.btnaddNewJobCard.Size = new System.Drawing.Size(177, 29);
            this.btnaddNewJobCard.TabIndex = 50;
            this.btnaddNewJobCard.Text = "Add New Card";
            this.btnaddNewJobCard.UseVisualStyleBackColor = true;
            this.btnaddNewJobCard.Click += new System.EventHandler(this.btnaddNewJobCard_Click);
            // 
            // tabChangePass
            // 
            this.tabChangePass.Controls.Add(this.login);
            this.tabChangePass.Location = new System.Drawing.Point(4, 25);
            this.tabChangePass.Name = "tabChangePass";
            this.tabChangePass.Padding = new System.Windows.Forms.Padding(3);
            this.tabChangePass.Size = new System.Drawing.Size(776, 505);
            this.tabChangePass.TabIndex = 5;
            this.tabChangePass.Text = "Change Password";
            this.tabChangePass.UseVisualStyleBackColor = true;
            // 
            // login
            // 
            this.login.Controls.Add(this.label6);
            this.login.Controls.Add(this.txtUserName);
            this.login.Controls.Add(this.label29);
            this.login.Controls.Add(this.txtConfirmPass);
            this.login.Controls.Add(this.label26);
            this.login.Controls.Add(this.btnChangePass);
            this.login.Controls.Add(this.lblPassword);
            this.login.Controls.Add(this.txtNewPass);
            this.login.Controls.Add(this.lblID);
            this.login.Controls.Add(this.txtOldPass);
            this.login.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.Location = new System.Drawing.Point(196, 55);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(384, 394);
            this.login.TabIndex = 28;
            this.login.TabStop = false;
            this.login.Text = "Change Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(64, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 18);
            this.label6.TabIndex = 35;
            this.label6.Text = "UserName";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(180, 134);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(158, 26);
            this.txtUserName.TabIndex = 34;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(34, 264);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 18);
            this.label29.TabIndex = 33;
            this.label29.Text = "Confirm Password:";
            // 
            // txtConfirmPass
            // 
            this.txtConfirmPass.Location = new System.Drawing.Point(180, 264);
            this.txtConfirmPass.Name = "txtConfirmPass";
            this.txtConfirmPass.Size = new System.Drawing.Size(158, 26);
            this.txtConfirmPass.TabIndex = 32;
            this.txtConfirmPass.UseSystemPasswordChar = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(16, 77);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(157, 19);
            this.label26.TabIndex = 31;
            this.label26.Text = "Auto Jalopy Motors";
            // 
            // btnChangePass
            // 
            this.btnChangePass.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChangePass.Location = new System.Drawing.Point(180, 335);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(158, 31);
            this.btnChangePass.TabIndex = 30;
            this.btnChangePass.Text = "Change";
            this.btnChangePass.UseVisualStyleBackColor = true;
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.Location = new System.Drawing.Point(56, 219);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(117, 18);
            this.lblPassword.TabIndex = 28;
            this.lblPassword.Text = "New Password:";
            // 
            // txtNewPass
            // 
            this.txtNewPass.Location = new System.Drawing.Point(180, 219);
            this.txtNewPass.Name = "txtNewPass";
            this.txtNewPass.Size = new System.Drawing.Size(158, 26);
            this.txtNewPass.TabIndex = 27;
            this.txtNewPass.UseSystemPasswordChar = true;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(64, 176);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(106, 18);
            this.lblID.TabIndex = 26;
            this.lblID.Text = "Old Password";
            // 
            // txtOldPass
            // 
            this.txtOldPass.Location = new System.Drawing.Point(180, 176);
            this.txtOldPass.Name = "txtOldPass";
            this.txtOldPass.Size = new System.Drawing.Size(158, 26);
            this.txtOldPass.TabIndex = 25;
            // 
            // btnLogOut2
            // 
            this.btnLogOut2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut2.Location = new System.Drawing.Point(637, 12);
            this.btnLogOut2.Name = "btnLogOut2";
            this.btnLogOut2.Size = new System.Drawing.Size(130, 30);
            this.btnLogOut2.TabIndex = 53;
            this.btnLogOut2.Text = "Log Out";
            this.btnLogOut2.UseVisualStyleBackColor = true;
            this.btnLogOut2.Click += new System.EventHandler(this.btnLogOut2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(281, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 19);
            this.label2.TabIndex = 52;
            this.label2.Text = "Auto Jalopy Motors: Staff";
            // 
            // dgvJobCardsStaff
            // 
            this.dgvJobCardsStaff.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvJobCardsStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobCardsStaff.Location = new System.Drawing.Point(17, 25);
            this.dgvJobCardsStaff.Name = "dgvJobCardsStaff";
            this.dgvJobCardsStaff.Size = new System.Drawing.Size(177, 401);
            this.dgvJobCardsStaff.TabIndex = 60;
            // 
            // frmStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnLogOut2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tabCarsStaff);
            this.Name = "frmStaff";
            this.Text = "Auto Jalopy Motors- Staff";
            this.Load += new System.EventHandler(this.frmStaff_Load);
            this.tabCarsStaff.ResumeLayout(false);
            this.tabCars.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustDetailsStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarsStaff)).EndInit();
            this.tabJobCardsAdm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShowJobCardStaff)).EndInit();
            this.tabChangePass.ResumeLayout(false);
            this.login.ResumeLayout(false);
            this.login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobCardsStaff)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabCarsStaff;
        private System.Windows.Forms.TabPage tabCars;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvCustDetailsStaff;
        private System.Windows.Forms.Label lblcar;
        private System.Windows.Forms.Button btnUpdateCarDetails;
        private System.Windows.Forms.DataGridView dgvCarsStaff;
        private System.Windows.Forms.Button btnUpdateCar;
        private System.Windows.Forms.TabPage tabJobCardsAdm;
        private System.Windows.Forms.Button btnaddNewJobCard;
        private System.Windows.Forms.TabPage tabChangePass;
        private System.Windows.Forms.GroupBox login;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtConfirmPass;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtNewPass;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtOldPass;
        private System.Windows.Forms.Button btnLogOut2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.DataGridView dgvShowJobCardStaff;
        private System.Windows.Forms.Button btnShowCardStaff;
        private System.Windows.Forms.DataGridView dgvJobCardsStaff;
    }
}