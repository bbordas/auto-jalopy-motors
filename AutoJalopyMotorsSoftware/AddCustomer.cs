﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmAddCustomer : Form
    {
        public frmAddCustomer()
        {
            InitializeComponent();
        }

        private void btnAddCustomer_Click_1(object sender, EventArgs e)
        {
            try
            {
                using (AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext())
                {
                    tbCustomer customer = new tbCustomer
                    {

                        CustomerID = int.Parse(txtCustomerID.Text),
                        FirstName = txtCustomerFirstName.Text,
                        LastName = txtCustomerLastName.Text,
                        HomeAddress = txtCustomerAddress.Text,
                        PhoneNumber = txtCustomerPhone.Text,
                        EmailAddress = TxtCustomerEmail.Text,
                        CouponCode = txtCustomerCoupon.Text,
                    };

                    AutoJDC.tbCustomers.InsertOnSubmit(customer);
                    AutoJDC.SubmitChanges();
                    DialogResult = DialogResult.OK;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = DialogResult.Cancel;

            }

        }

        private void btnCustCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }      
}
