﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmStaff : Form
    {
   
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();

        public frmStaff()
        {
            InitializeComponent();
        }

        private void btnLogOut2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmStaff_Load(object sender, EventArgs e)
        {

            var RegNumQuery = (
                   from a in AutoJDC.tbCars
                   select new { a.CarID, RegNum = a.RegNum })
                   .ToList();


            dgvCarsStaff.AutoGenerateColumns = false;
            dgvCustDetailsStaff.AutoGenerateColumns = false;
            dgvJobCardsStaff.AutoGenerateColumns = false;
            dgvShowJobCardStaff.AutoGenerateColumns = false;

            dgvCarsStaff.ColumnCount = 7;

            dgvCarsStaff.Columns[0].HeaderText = "Car ID";
            dgvCarsStaff.Columns[0].DataPropertyName = "CarID";

            dgvCarsStaff.Columns[1].HeaderText = "Reg Number";
            dgvCarsStaff.Columns[1].DataPropertyName = "RegNum";

            dgvCarsStaff.Columns[2].HeaderText = "Make";
            dgvCarsStaff.Columns[2].DataPropertyName = "Make";

            dgvCarsStaff.Columns[3].HeaderText = "Model";
            dgvCarsStaff.Columns[3].DataPropertyName = "Model";

            dgvCarsStaff.Columns[4].HeaderText = "Color";
            dgvCarsStaff.Columns[4].DataPropertyName = "Color";

            dgvCarsStaff.Columns[5].HeaderText = "NCT Due";
            dgvCarsStaff.Columns[5].DataPropertyName = "NCT_Due";

            dgvCarsStaff.Columns[6].HeaderText = "Customer ID";
            dgvCarsStaff.Columns[6].DataPropertyName = "CustomerID";


            dgvCustDetailsStaff.ColumnCount = 4;

            dgvCustDetailsStaff.Columns[0].HeaderText = "ID";
            dgvCustDetailsStaff.Columns[0].DataPropertyName = "CustomerID";

            dgvCustDetailsStaff.Columns[1].HeaderText = "First Name";
            dgvCustDetailsStaff.Columns[1].DataPropertyName = "FirstName";

            dgvCustDetailsStaff.Columns[2].HeaderText = "Lastname";
            dgvCustDetailsStaff.Columns[2].DataPropertyName = "LastName";

            dgvCustDetailsStaff.Columns[3].HeaderText = "Contact";
            dgvCustDetailsStaff.Columns[3].DataPropertyName = "PhoneNumber";


            dgvJobCardsStaff.ColumnCount = 1;

            dgvJobCardsStaff.Columns[0].HeaderText = "Card ID";
            dgvJobCardsStaff.Columns[0].DataPropertyName = "CardID";

            dgvShowJobCardStaff.ColumnCount = 7;

            dgvShowJobCardStaff.Columns[0].HeaderText = "Card ID";
            dgvShowJobCardStaff.Columns[0].DataPropertyName = "CardID";

            dgvShowJobCardStaff.Columns[1].HeaderText = "Start Date";
            dgvShowJobCardStaff.Columns[1].DataPropertyName = "StartDate";

            dgvShowJobCardStaff.Columns[2].HeaderText = "End Date";
            dgvShowJobCardStaff.Columns[2].DataPropertyName = "EndDate";

            dgvShowJobCardStaff.Columns[3].HeaderText = "Reg. Number";
            dgvShowJobCardStaff.Columns[3].DataPropertyName = "RegNum";

            dgvShowJobCardStaff.Columns[4].HeaderText = "Description";
            dgvShowJobCardStaff.Columns[4].DataPropertyName = "JobDescription";

            dgvShowJobCardStaff.Columns[5].HeaderText = "Customer";
            dgvShowJobCardStaff.Columns[5].DataPropertyName = "LastName";

            dgvShowJobCardStaff.Columns[6].HeaderText = "Mechanic";
            dgvShowJobCardStaff.Columns[6].DataPropertyName = "StaffLastName";



            PrimeGrid();

        }

        private void PrimeGrid()
        {
            dgvCarsStaff.DataSource = null;
                var cars = from c in AutoJDC.tbCars
                       select c;
                dgvCarsStaff.DataSource = cars;


            dgvJobCardsStaff.DataSource = null;
            var jobcard = (from card in AutoJDC.tbJobCards
                           select card);



                dgvJobCardsStaff.DataSource = jobcard;

            dgvShowJobCardStaff.DataSource = null;
        }


// Cars page *****************************************************************************************


        private int GetCarStaffID(int rowIndex)
        {
            return Convert.ToInt32(dgvCarsStaff.Rows[rowIndex].Cells[0].Value);
        }

        private int GetCurrentCarStaffIndex()
        {
            int rowIndex = dgvCarsStaff.CurrentRow.Index;
            return rowIndex;
        }


        private void dgvCarsStaff_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int rowIndex = GetCurrentCarStaffIndex();
            int carID = GetCarStaffID(rowIndex);

            dgvCustDetailsStaff.DataSource = (from c in AutoJDC.tbCars
                                              join cust in AutoJDC.tbCustomers on c.CustomerID equals cust.CustomerID
                                              where c.CarID == carID
                                              select cust
                                         ).ToList();
        }

        private void btnUpdateCarDetails_Click(object sender, EventArgs e)
        {
            int rowIndex = GetCurrentCarStaffIndex();
            int selectedCarID = GetCarStaffID(rowIndex);

            var updated = (from cars in AutoJDC.tbCars
                           join c in AutoJDC.tbCustomers on cars.CustomerID equals c.CustomerID
                           where cars.CarID == selectedCarID
                           select cars).First();

            if (updated != null)
            {

                updated.Color = Convert.ToString(dgvCarsStaff.Rows[rowIndex].Cells[4].Value);
                updated.NCT_Due = Convert.ToDateTime(dgvCarsStaff.Rows[rowIndex].Cells[5].Value);

                PrimeGrid();

                try
                {
                    AutoJDC.SubmitChanges();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }



// JObCard Page************************************************************************************



        private int GetCardStaffID(int rowIndex)
        {
            return Convert.ToInt32(dgvJobCardsStaff.Rows[rowIndex].Cells[0].Value);
        }

        private int GetCurrentCardStaffIndex()
        {
            int rowIndex = dgvJobCardsStaff.CurrentRow.Index;
            return rowIndex;
        }


        private void btnaddNewJobCard_Click(object sender, EventArgs e)
        {
            using (frmAddCard addCard = new frmAddCard())
            {
                DialogResult dlgResult = addCard.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }
        }


        private void btnShowCardStaff_Click(object sender, EventArgs e)
        {
            dgvShowJobCardStaff.DataSource = (from job in AutoJDC.tbJobs
                                  join card in AutoJDC.tbJobCards on job.JobID equals card.JobID
                                  join car in AutoJDC.tbCars on card.CarID equals car.CarID
                                  join cust2 in AutoJDC.tbCustomers on car.CustomerID equals cust2.CustomerID
                                  join staff in AutoJDC.tbStaffs on card.StaffID equals staff.StaffID

                                  select new { card.CardID, card.StartDate, card.EndDate, job.JobDescription, car.RegNum, cust2.LastName, staff.StaffLastName });
        }

  


// Password page**************************************************************************************8



        private void btnChangePass_Click(object sender, EventArgs e)
        {
            ChangePassword();
        }


        public void ChangePassword()
        {
            string userName = txtUserName.ToString();
            string password = txtOldPass.ToString();
            string newPass = txtNewPass.ToString();
            string confirmPass = txtConfirmPass.ToString();

           try
                {
                    var oldpass = (from pass in AutoJDC.tbStaffs
                                   where pass.StaffEmail == userName && pass.StaffPassword == password
                                   select pass.StaffPassword);

                    if (oldpass != null && newPass == confirmPass)
                    {
                        try
                        {
                            tbStaff staff = new tbStaff
                            {

                                StaffPassword = txtConfirmPass.ToString(),

                            };

                            AutoJDC.SubmitChanges();
                            MessageBox.Show("Your password has been changed");
                            DialogResult = DialogResult.OK;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else MessageBox.Show("Passwords don't match");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            

        }

       
    }
}
