﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmAddNewCar : Form
    {
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();

        public frmAddNewCar()
        {
            InitializeComponent();
        }

        private void frmAddNewCar_Load(object sender, EventArgs e)
        {
            var cust = (from a in AutoJDC.tbCustomers
                                           select new { a.CustomerID, Name = a.CustomerID + " - "+ a.LastName });

            cmbCustomerIDCar.DataSource = cust;
            cmbCustomerIDCar.DisplayMember = "Name";
            cmbCustomerIDCar.ValueMember = "CustomerID";
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {

            try
            {
                tbCar car = new tbCar
                {
                    CarID = int.Parse(txtCarId.Text),
                    RegNum = txtCarRegNum.Text,
                    Make = txtCarMake.Text,
                    Model = txtCarModel.Text,
                    Color = txtCarColor.Text,
                    NCT_Due = DateTime.Parse(dtpNCTCar.Text),
                    CustomerID = int.Parse(cmbCustomerIDCar.SelectedValue.ToString())
                };
         
                AutoJDC.tbCars.InsertOnSubmit(car);
                AutoJDC.SubmitChanges();
                DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = DialogResult.Cancel;

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }


    }
}
