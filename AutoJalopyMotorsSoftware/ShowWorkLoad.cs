﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmShowWorkLoad : Form
    {
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();

        public frmShowWorkLoad()
        {
            InitializeComponent();

           
        }

        private void frmShowWorkLoad_Load(object sender, EventArgs e)
        {

            this.cmbStaffI.DataSource = AutoJDC.tbStaffs.Select(a => a.StaffID)
                                                    .Distinct()
                                                    .Select(ac => ac);

            dgvWorkLoad.ColumnCount = 5;

            dgvWorkLoad.Columns[0].HeaderText = "ID";
            dgvWorkLoad.Columns[0].DataPropertyName = "StaffID";

            dgvWorkLoad.Columns[1].HeaderText = "Lastname";
            dgvWorkLoad.Columns[1].DataPropertyName = "StaffLastName";

            dgvWorkLoad.Columns[2].HeaderText = "Description";
            dgvWorkLoad.Columns[2].DataPropertyName = "JobDescription";

            dgvWorkLoad.Columns[3].HeaderText = "Start";
            dgvWorkLoad.Columns[3].DataPropertyName = "StartDate";

            dgvWorkLoad.Columns[4].HeaderText = "End";
            dgvWorkLoad.Columns[4].DataPropertyName = "EndDate";
        }

        private void btnWorkLoadStaff_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedId = int.Parse(cmbStaffI.SelectedValue.ToString());

                dgvWorkLoad.DataSource = (from st in AutoJDC.tbStaffs
                                   join jc in AutoJDC.tbJobCards on st.StaffID equals jc.StaffID
                                   join j in AutoJDC.tbJobs on jc.JobID equals j.JobID
                                   where (selectedId == st.StaffID) && (jc.StartDate >= dtpStartWL.Value && jc.EndDate <= dtpEndWL.Value) 
                                   orderby jc.StartDate
                                   select new { st.StaffID, st.StaffLastName, j.JobDescription, jc.StartDate, jc.EndDate }).ToList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        
    }
}
