﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class Admin : Form
    {
        bool NewRowMode = false;
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();


        public Admin()
        {
            InitializeComponent();
        }

        private void btnLogOut2_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void Admin_Load(object sender, EventArgs e)
        {
            var RegNumQuery = (
                   from a in AutoJDC.tbCars
                   select new { a.CarID, RegNum = a.RegNum })
                   .ToList();

            dgvCars.AutoGenerateColumns = false;
            dgvCustomer.AutoGenerateColumns = false;
            dgvJobCards.AutoGenerateColumns = false;
            dgvCustDetails.AutoGenerateColumns = false;
            dgvShow.AutoGenerateColumns = false;

            dgvCars.ColumnCount = 7;

            dgvCars.Columns[0].HeaderText = "Car ID";
            dgvCars.Columns[0].DataPropertyName = "CarID";

            dgvCars.Columns[1].HeaderText = "Reg Number";
            dgvCars.Columns[1].DataPropertyName = "RegNum";

            dgvCars.Columns[2].HeaderText = "Make";
            dgvCars.Columns[2].DataPropertyName = "Make";

            dgvCars.Columns[3].HeaderText = "Model";
            dgvCars.Columns[3].DataPropertyName = "Model";

            dgvCars.Columns[4].HeaderText = "Color";
            dgvCars.Columns[4].DataPropertyName = "Color";

            dgvCars.Columns[5].HeaderText = "NCT Due";
            dgvCars.Columns[5].DataPropertyName = "NCT_Due";

            dgvCars.Columns[6].HeaderText = "Customer ID";
            dgvCars.Columns[6].DataPropertyName = "CustomerID";


            dgvCustomer.ColumnCount = 7;

            dgvCustomer.Columns[0].HeaderText = "ID";
            dgvCustomer.Columns[0].DataPropertyName = "CustomerID";

            dgvCustomer.Columns[1].HeaderText = "First Name";
            dgvCustomer.Columns[1].DataPropertyName = "FirstName";

            dgvCustomer.Columns[2].HeaderText = "Lastname";
            dgvCustomer.Columns[2].DataPropertyName = "LastName";

            dgvCustomer.Columns[3].HeaderText = "Address";
            dgvCustomer.Columns[3].DataPropertyName = "HomeAddress";

            dgvCustomer.Columns[4].HeaderText = "Contact";
            dgvCustomer.Columns[4].DataPropertyName = "PhoneNumber";

            dgvCustomer.Columns[5].HeaderText = "Email";
            dgvCustomer.Columns[5].DataPropertyName = "EmailAddress";

            dgvCustomer.Columns[6].HeaderText = "Voucher";
            dgvCustomer.Columns[6].DataPropertyName = "CouponCode";


            dgvCustDetails.ColumnCount = 4;

            dgvCustDetails.Columns[0].HeaderText = "ID";
            dgvCustDetails.Columns[0].DataPropertyName = "CustomerID";

            dgvCustDetails.Columns[1].HeaderText = "First Name";
            dgvCustDetails.Columns[1].DataPropertyName = "FirstName";

            dgvCustDetails.Columns[2].HeaderText = "Lastname";
            dgvCustDetails.Columns[2].DataPropertyName = "LastName";

            dgvCustDetails.Columns[3].HeaderText = "Contact";
            dgvCustDetails.Columns[3].DataPropertyName = "PhoneNumber";



            dgvJobCards.ColumnCount = 1;

            dgvJobCards.Columns[0].HeaderText = "Card ID";
            dgvJobCards.Columns[0].DataPropertyName = "CardID";


            dgvShow.ColumnCount = 7;

            dgvShow.Columns[0].HeaderText = "Card ID";
            dgvShow.Columns[0].DataPropertyName = "CardID";

            dgvShow.Columns[1].HeaderText = "Start Date";
            dgvShow.Columns[1].DataPropertyName = "StartDate";

            dgvShow.Columns[2].HeaderText = "End Date";
            dgvShow.Columns[2].DataPropertyName = "EndDate";

            dgvShow.Columns[3].HeaderText = "Reg. Number";
            dgvShow.Columns[3].DataPropertyName = "RegNum";

            dgvShow.Columns[4].HeaderText = "Description";
            dgvShow.Columns[4].DataPropertyName = "JobDescription";

            dgvShow.Columns[5].HeaderText = "Customer";
            dgvShow.Columns[5].DataPropertyName = "LastName";

            dgvShow.Columns[6].HeaderText = "Mechanic";
            dgvShow.Columns[6].DataPropertyName = "StaffLastName";

            PrimeGrid();
        }



        private void PrimeGrid()
        {
            dgvStaff.DataSource = null;
            var s = from c in AutoJDC.tbStaffs
                    select c;
            dgvStaff.DataSource = s;


            dgvCustomer.DataSource = null;
            var cust = from c in AutoJDC.tbCustomers
                       select c;
            dgvCustomer.DataSource = cust;


            dgvCars.DataSource = null;
            var cars = from c in AutoJDC.tbCars
                       select c;
            dgvCars.DataSource = cars;

            dgvJobCards.DataSource = null;

            var jobcard = (from card in AutoJDC.tbJobCards                           
                          select card);

            dgvJobCards.DataSource = jobcard;




        }

        //Staff page *******************************************************************        



        private void dgvStaff_RowLeave_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == dgvStaff.NewRowIndex - 1 && NewRowMode)
            {
                try
                {
                    tbStaff staff = new tbStaff
                    {
                        StaffFirstName = Convert.ToString(dgvStaff.Rows[e.RowIndex].Cells[1].Value),
                        StaffLastName = Convert.ToString(dgvStaff.Rows[e.RowIndex].Cells[2].EditedFormattedValue)
                    };

                    AutoJDC.tbStaffs.InsertOnSubmit(staff);
                    AutoJDC.SubmitChanges();
                    NewRowMode = false;
                    dgvStaff.DataSource = AutoJDC.tbStaffs.GetNewBindingList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }



       
        private void btnStaffWorkLoad_Click(object sender, EventArgs e)
        {
            using (frmShowWorkLoad workload = new frmShowWorkLoad())
            {
                DialogResult dlgResult = workload.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }
        }


        private void btnAddStaff_Click(object sender, EventArgs e)
        {
            using (frmAddStaff staff = new frmAddStaff())
            {
                DialogResult dlgResult = staff.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }

        }



        private int GetStaffID(int rowIndex)
        {
            return Convert.ToInt32(dgvStaff.Rows[rowIndex].Cells[0].Value);
        }


        private int GetCurrentStaffIndex()
        {
            int rowIndex = dgvStaff.CurrentRow.Index;

            return rowIndex;
        }

        private void btnUpdateStaff_Click(object sender, EventArgs e)
        {
            int rowIndex = GetCurrentStaffIndex();
            int id = GetStaffID(rowIndex);

            var updated = (from c in AutoJDC.tbStaffs
                           where c.StaffID == id
                           select c).First();

            if (updated != null)
            {
                updated.StaffID = Convert.ToInt32(dgvCustomer.Rows[rowIndex].Cells[0].Value);
                updated.StaffFirstName = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[1].Value);
                updated.StaffLastName = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[2].Value);
                updated.StaffAddress = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[3].Value);
                updated.StaffPhone = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[4].Value);
                updated.StaffEmail = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[5].Value);
                updated.AccessLevel = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[6].Value);
                updated.StaffPassword = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[6].Value);

                PrimeGrid();

                try
                {
                    AutoJDC.SubmitChanges();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
               

                }
            }
        }


 //Customer page *******************************************************************     


        private int GetCustomerID(int rowIndex)
        {
            return Convert.ToInt32(dgvCustomer.Rows[rowIndex].Cells[0].Value);
        }


        private int GetCurrentCustomerIndex()
        {
            int rowIndex = dgvCustomer.CurrentRow.Index;
            return rowIndex;
        }


        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            using (frmAddCustomer addCust = new frmAddCustomer())
            {
                DialogResult dlgResult = addCust.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }
        }


        private void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            int rowIndex = GetCurrentCustomerIndex();
            int id = GetCustomerID(rowIndex);

            var updated = (from cars in AutoJDC.tbCars
                           join c in AutoJDC.tbCustomers on cars.CustomerID equals c.CustomerID
                           where cars.CarID == id
                           select cars).First();

            if (updated != null)
            {

                updated.Color = Convert.ToString(dgvCars.Rows[rowIndex].Cells[4].Value);
                updated.NCT_Due = Convert.ToDateTime(dgvCars.Rows[rowIndex].Cells[5].Value);

                PrimeGrid();

                try
                {
                    AutoJDC.SubmitChanges();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;

                }
            }
        }



//Cars page **********************************************************************************


        private int GetCurrentCarIndex()
        {
            int rowIndex = dgvCars.CurrentRow.Index;

            return rowIndex;
        }

        private int GetCarID(int rowIndex)
        {
            return Convert.ToInt32(dgvCars.Rows[rowIndex].Cells[0].Value);
        }

        private void dgvCars_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = GetCurrentCarIndex();
            int carID = GetCarID(rowIndex);

            dgvCustDetails.DataSource = (from c in AutoJDC.tbCars
                                         join cust in AutoJDC.tbCustomers on c.CustomerID equals cust.CustomerID
                                         where c.CarID == carID
                                         select cust
                                         ).ToList();
        }


        private void btnAddCar_Click(object sender, EventArgs e)
        {
            using (frmAddNewCar car = new frmAddNewCar())
            {
                DialogResult dlgResult = car.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }

        }


        private void btnUpdateCarDetails_Click(object sender, EventArgs e)
        {
            int rowIndex = GetCurrentCarIndex();
            int id = GetCarID(rowIndex);

            var updated = (from c in AutoJDC.tbCars
                           where c.CarID == id
                           select c).First();

            if (updated != null)
            {
                updated.CarID = Convert.ToInt32(dgvCustomer.Rows[rowIndex].Cells[0].Value);
                updated.RegNum = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[1].Value);
                updated.Make = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[2].Value);
                updated.Model = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[3].Value);
                updated.Color = Convert.ToString(dgvCustomer.Rows[rowIndex].Cells[4].Value);
                updated.NCT_Due = Convert.ToDateTime(dgvCustomer.Rows[rowIndex].Cells[5].Value);
                updated.CustomerID = Convert.ToInt32(dgvCustomer.Rows[rowIndex].Cells[6].Value);

                PrimeGrid();

                try
                {
                    AutoJDC.SubmitChanges();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                   

                }
            }
        }





//Card Page*******************************************************************


        private int GetCardID(int rowIndex)
        {
            return Convert.ToInt32(dgvJobCards.Rows[rowIndex].Cells[0].Value);
        }


        private int GetCurrentCardIndex()
        {
            int rowIndex = dgvJobCards.CurrentRow.Index;

            return rowIndex;
        }


        private void btnaddNewJobCard_Click(object sender, EventArgs e)
        {
            using (frmAddCard card = new frmAddCard())
            {
                DialogResult dlgResult = card.ShowDialog();

                if (dlgResult == DialogResult.OK)
                {
                    PrimeGrid();
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            dgvShow.DataSource = (from job in AutoJDC.tbJobs
                                  join card in AutoJDC.tbJobCards on job.JobID equals card.JobID
                                  join car in AutoJDC.tbCars on card.CarID equals car.CarID
                                  join cust2 in AutoJDC.tbCustomers on car.CustomerID equals cust2.CustomerID
                                  join staff in AutoJDC.tbStaffs on card.StaffID equals staff.StaffID
                                 
                                  select new { card.CardID, card.StartDate, card.EndDate, job.JobDescription, car.RegNum, cust2.LastName, staff.StaffLastName });
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int rowIndex = GetCurrentCardIndex();

            int id = GetCardID(rowIndex);


            var jobCardDelete = (from card in AutoJDC.tbJobCards
                                  where card.CardID == id
                                  select card).First();

            if (jobCardDelete != null)
            {
                AutoJDC.tbJobCards.DeleteOnSubmit(jobCardDelete);
                AutoJDC.SubmitChanges();
            }

            PrimeGrid();
        }




//Report page *********************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            dgvReport.DataSource = null;

            dgvReport.DataSource = AutoJDC.tbCars.Select(r => new { r.Make, r.Model }).Distinct();

        }


        private void btnReportWorkLoad_Click(object sender, EventArgs e)
        {

            dgvReport.DataSource = null;

            dgvReport.ColumnCount = 6;

            dgvReport.Columns[0].HeaderText = "F Name";
            dgvReport.Columns[0].DataPropertyName = "StaffFirstName";

            dgvReport.Columns[1].HeaderText = "L Name";
            dgvReport.Columns[1].DataPropertyName = "StaffLastName";

            dgvReport.Columns[2].HeaderText = "Start";
            dgvReport.Columns[2].DataPropertyName = "StartDate";

            dgvReport.Columns[3].HeaderText = "End";
            dgvReport.Columns[3].DataPropertyName = "EndDate";

            dgvReport.Columns[4].HeaderText = "Description";
            dgvReport.Columns[4].DataPropertyName = "JobDescription";

            dgvReport.DataSource = (from jc in AutoJDC.tbJobCards
                                    join st in AutoJDC.tbStaffs on jc.StaffID equals st.StaffID
                                    join j in AutoJDC.tbJobs on jc.JobID equals j.JobID
                                    where jc.StartDate >= dtpStartReport.Value && jc.EndDate <= dtpEndReport.Value
                                    orderby st.StaffLastName
                                    select new { st.StaffFirstName, st.StaffLastName, jc.StartDate, jc.EndDate, j.JobDescription }
                                                ).ToList();
        }

        private void btnReportJobHistory_Click(object sender, EventArgs e)
        {

            dgvReport.DataSource = null;

            dgvReport.ColumnCount = 4;

            dgvReport.Columns[0].HeaderText = "Card ID";
            dgvReport.Columns[0].DataPropertyName = "CardID";

            dgvReport.Columns[1].HeaderText = "Description";
            dgvReport.Columns[1].DataPropertyName = "JobDescription";

            dgvReport.Columns[2].HeaderText = "Start";
            dgvReport.Columns[2].DataPropertyName = "StartDate";

            dgvReport.Columns[3].HeaderText = "End";
            dgvReport.Columns[3].DataPropertyName = "EndDate";


            dgvReport.DataSource = null;
            dgvReport.DataSource = (from jc in AutoJDC.tbJobCards
                                    join j in AutoJDC.tbJobs on jc.JobID equals j.JobID
                                    where jc.StartDate >= dtpStartReport.Value && jc.EndDate <= dtpEndReport.Value
                                    orderby jc.StartDate
                                    select new { jc.CardID, j.JobDescription, jc.StartDate, jc.EndDate }).ToList();
        }




//Change Password page****************************************************************



        private void btnChangePass_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.ToString();
            string password = txtOldPass.ToString();
            string newPass = txtNewPass.ToString();
            string confirmPass = txtConfirmPass.ToString();

           
                try
                {
                    var oldpass = (from pass in AutoJDC.tbStaffs 
                                   where pass.StaffEmail == userName && pass.StaffPassword == password
                                   select pass.StaffPassword);

                    if (oldpass != null && newPass == confirmPass)
                    {
                        try
                        {
                            tbStaff staff = new tbStaff
                            {

                                StaffPassword = txtConfirmPass.ToString(),

                            };

                            AutoJDC.SubmitChanges();
                            MessageBox.Show("Your password has been changed");
                            DialogResult = DialogResult.OK;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else MessageBox.Show("Passwords don't match");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

      
    }

      
    }



