﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoJalopyMotorsSoftware
{
    public partial class frmAddCard : Form
    {
        AutoJalopyDataContext AutoJDC = new AutoJalopyDataContext();


        public frmAddCard()
        {
            InitializeComponent();
        }

        private void frmAddCard_Load(object sender, EventArgs e)
        {
            var JobCardQuery = (
            from a in AutoJDC.tbJobCards
            join j in AutoJDC.tbJobs on a.JobID equals j.JobID
            select new { a.CardID, card = j.JobDescription })
            .ToList();

            cmbJob.DataSource = JobCardQuery;
            cmbJob.DisplayMember = "card";
            cmbJob.ValueMember = "CardID";

            var RegNumQuery = (
                       from a in AutoJDC.tbCars
                       select new { a.CarID, RegNum = a.RegNum })
                       .ToList();

            cmbRegNum.DataSource = RegNumQuery;
            cmbRegNum.DisplayMember = "RegNum";
            cmbRegNum.ValueMember = "CarID";

            var StaffNamesQuery = (
                         from a in AutoJDC.tbStaffs
                         select new { a.StaffID, Names = a. StaffLastName})
                         .ToList();

            cmbMechanic.DataSource = StaffNamesQuery;
            cmbMechanic.DisplayMember = "Names";
            cmbMechanic.ValueMember = "StaffID";
        }

        private void btnAddJobCard_Click(object sender, EventArgs e)
        {
            try
            {
                tbJobCard jobCard = new tbJobCard
                {
                    CardID = Int32.Parse(txtJObCardID.Text),
                    StartDate = DateTime.Parse(dtpStartDate.Text),
                    EndDate = DateTime.Parse(dtpStartDate.Text),
                    JobID = Convert.ToInt32(cmbJob.SelectedValue.ToString()),
                    StaffID = Convert.ToInt32(cmbJob.SelectedValue.ToString())
                };

                AutoJDC.tbJobCards.InsertOnSubmit(jobCard);
                AutoJDC.SubmitChanges();
                DialogResult = DialogResult.OK;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = DialogResult.Cancel;

            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }      
    }
}
