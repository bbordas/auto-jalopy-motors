﻿CREATE PROC uspInsertInto_tbCars
AS
INSERT INTO tbCars (CarID, RegNum, Make, Model, Color, NCT_Due, CustomerID)
VALUES (1,'07-D-12345','Renault','Megane','Red',  CAST(N'2016-10-12T00:00:00.000' AS DateTime),1 ),
 (2, '11-D-12389','Ford','Ka','Red',  CAST(N'2016-10-13T00:00:00.000' AS DateTime), 1),
 (3, '12-D-12548','Toyota','Avensis','Blue', CAST(N'2016-10-13T00:00:00.000' AS DateTime), 2),
 (4, '12-D-12645','Fiat','Panda','Yellow', CAST(N'2016-10-14T00:00:00.000' AS DateTime), 3),
 (5, '12-D-21478','Renault','Clio','Silver', CAST(N'2016-11-15T00:00:00.000' AS DateTime),4),
 (6, '13-D-25445','Ford','Escort','Black',CAST(N'2016-11-15T00:00:00.000' AS DateTime), 5),
 (7, '14-D12548','Fiat','Punto','Silver', CAST(N'2016-11-15T00:00:00.000' AS DateTime),5)