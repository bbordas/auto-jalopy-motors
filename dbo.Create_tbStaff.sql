﻿CREATE PROC Create_tbStaff
AS
CREATE TABLE tbStaff(
StaffID int not null PRIMARY KEY,
StaffFirstName varchar (100),
StaffLastName varchar(50),
StaffAddress varchar(200),
StaffPhone varchar (20),
StaffEmail varchar(50),
StaffPassword varchar(50),
AccessLevel varchar(10)
)